#! /bin/bash

if [ ! -e logs ]; then 
	echo "logs directory does not exist, creating..."
	/bin/mkdir -p logs;
	chmod -fR 777 logs;
fi

if [ ! -e cache ]; then 
	echo "cache directory does not exist, creating..."
	/bin/mkdir -p cache;
	chmod -fR 777 cache;
fi

if [ ! -e bin ]; then 
	echo "bin directory does not exist, creating..."
	/bin/mkdir -p bin;
fi

if [ ! -e app ]; then 
	echo "app directory does not exist, creating..."
	cp -R vendor/llim/silexmonkey-core/app app;
fi

if [ ! -e tests ]; then 
	echo "test directory does not exist, creating..."
	cp -R vendor/llim/silexmonkey-core/tests tests;
fi

if [ ! -e features ]; then 
	echo "features (for behat)  directory does not exist, creating..."
	cp -R vendor/llim/silexmonkey-core/features features;
fi

if [ ! -e config ]; then 
	echo "config directory does not exist, creating..."
	cp -R vendor/llim/silexmonkey-core/config config;
fi

if [ ! -e web ]; then 
	echo "web directory does not exist, creating..."
	cp -R vendor/llim/silexmonkey-core/web web;
fi

if [ ! -e views ]; then 
	echo "views directory does not exist, creating..."
	cp -R vendor/llim/silexmonkey-core/views views;
fi
