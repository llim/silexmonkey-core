#! /bin/bash

echo "rebuild cache directory!";
/bin/rm -rf cache;
if [ ! -e cache ]; then 
	/bin/mkdir -p cache;
	chmod -fR 777 cache;
fi
