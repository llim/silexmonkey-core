server {
    set $root "/var/www/awesome";
    access_log  /var/log/nginx/awesome-access.log ;
    error_log  /var/log/nginx/awesome-error.log ;

    server_name awesome.silexmonkey.com;
    root $root;

    location ~* \.(jpg|jpeg|gif|png|css|js|ico)$ {
        root /var/www/awesome/web;
        access_log      off;
        log_not_found   off;
        expires         360d;
    }

    location ~* /(css|js|fonts|img)/* {
        root /var/www/awesome/web;
        access_log      off;
        log_not_found   off;
        expires         360d;
    }

    #site root is redirected to the app boot script
    location = / {
        try_files @site @site;
    }

    #all other locations try other files first and go to our front controller if none of them exists
    location / {
        try_files $uri $uri/ @site;
    }

    #return 404 for all php files as we do have a front controller
    location ~ \.php$ {
        return 404;
    }

    location = /utils/apc.php {
        fastcgi_pass 127.0.0.1:9001;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME $root/web/utils/apc.php;
    }

    location @site {
        fastcgi_pass 127.0.0.1:9001;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME $root/web/index.php;
        fastcgi_param APP_DEBUG 0;
        fastcgi_param APP_ENV prod; 
    }
}
