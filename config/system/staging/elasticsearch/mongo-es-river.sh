#! /bin/bash

curl -XPUT 'http://localhost:9200/_river/creditmaster/_meta' -d '{
    "type": "mongodb", 
    "mongodb": { 
	"servers": [{"host":"127.0.0.1","port":27017}],
        "db": "awesome", 
        "collection": "stuffs", 
        "gridfs": false
    }, 
    "index": { 
        "name": "awesome", 
        "type": "stuffs" 
    }
}'
