<?php 

require './vendor/autoload.php';

$queue = $argv[1];
$mongoServer = 'localhost';
$mongoPort = 27017;
$mongoDb = 'sample';
$redisServer = 'localhost';
$redisPort = 6379;

$baseSleepTime = 5;
$maxSleepTime = 160;

$qMaster = new QueueMaster($mongoServer, $mongoPort, $mongoDb, $redisServer, $redisPort, $queue);
$worker = new QueueWorker( $qMaster );

$worker->working();
