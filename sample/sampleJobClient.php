<?php

include './test.php';

$mongoServer = 'localhost';
$mongoPort = 27017;
$mongoDb = 'sample';
$redisServer = 'localhost';
$redisPort = 6379;
$queue = 'testQueue';

$qMaster = new QueueMaster($mongoServer, $mongoPort, $mongoDb, $redisServer, $redisPort, $queue);

for($n=0;$n<=1000;$n++){

    $jobClass = (rand(1,10) >= 2) ? 'RealJob' : 'RealPickyJob' ;
    
    $qMaster->addJob($jobClass, array('blah' => 'foo0', 'time' => time()),array(
            array('class' => 'RealJob', 'payload' => array('blah' => 'foo1')),
            )
    );

    sleep( rand(1,3) );
}
