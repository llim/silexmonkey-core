<?php

namespace SilexMonkey;

use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

class App
{
    public static function boot($appPath, $consoleMode = false)
    {
        $start = microtime(true);
        $env = getenv('APP_ENV');
        $debug = ( ($env != 'prod') && ('cli' !== php_sapi_name()) );
        ErrorHandler::register(true);
        ExceptionHandler::register(true);

        if( $debug ) {
            Debug::enable(null, $debug);
        }

        $app = new \Silex\Application();

        $app['contextData'] = array(); // contain request context information
        $app['formData'] = array(); // contain form data submitted in this request
        $app['inputData'] = array(); // processed form data, removed non-model related information, such as submit, _access_token, _page, _limit, etc
        $app['outputData'] = array(); // this is the data used for generating template, or JSON output
        $app['trackingData'] = array('startTS' => $start); // this is the tracking data
        $app['appData'] = array(
                    'error' => '',
                    'statusCode' => 200,
                    'jsonOut' => false,
                    'htmlOut' => true
        ); // this is for storing application process data, such as error, status code

        $app['debug'] = $debug;
        $app['env'] = $env ?: 'prod';
        $app['app_path'] = $appPath ;

        $app->register(
            new \Herrera\Wise\WiseServiceProvider(),
            array(
                'wise.cache_dir' => $app['app_path']. '/cache/config',
                'wise.path' => $app['app_path'] . '/config/app/'.$app['env'],
                'wise.options' => array(
                    'type' => 'yml',
                    'mode' => $app['env'],
                    'config' => array(
                        'services' => 'services',
                        'routes' => 'routes',
                        ),
                    'parameters' => $app
                )
            )
        );

        $app['config'] = $app['wise']->load('app.yml');
        $app['search'] = $app['wise']->load('search.yml')['search'];
        $app['oauth'] = $app['wise']->load('oauth.yml')['oauth'];
        //Register service provider into Silex: config/<env>/services.yml
        \Herrera\Wise\WiseServiceProvider::registerServices($app);
        //Register Routes: config/<env>/routes.yml
        \Herrera\Wise\WiseServiceProvider::registerRoutes($app);

        $app['txid'] = new \MongoId();

        // Trusted the Forwarded host information in HTTP header, if it is coming from internal IP
        // This will allow situation in AWS which machines are behind ELB
        \Symfony\Component\HttpFoundation\Request::setTrustedProxies(array('10.0.0.0/8', '192.168.0.0/16'));
        $app['request'] = \Symfony\Component\HttpFoundation\Request::createFromGlobals();;

        if ( ! $consoleMode ) {
            $app->register(new \Silex\Provider\SessionServiceProvider());
            $app->register(new \Silex\Provider\UrlGeneratorServiceProvider());
            $app->register(new \Silex\Provider\FormServiceProvider());

            // Not to use default Silex Session (symfony) session handler, use PHP default
            $app['session.storage.handler'] = null;
            $sessionLifeTime = $app['config']['control']['sessionLifeTime'];
            $sessionLifeTime =  !empty($sessionLifeTime) ? $sessionLifeTime : 7200;
            $app["session.storage.options"] = ["cookie_lifetime"=>$sessionLifeTime];

            if ( !$app['session']->isStarted() ) {
                $app['session']->start();
            }

            //B-cookie ==> Browser instance cookie, to track uniq instance of browser
            if($app['request']->cookies->has("b")) {
                $app['bcookie'] = $app['request']->cookies->get('b');
            } else {
                $app['bcookie'] = uniqid();
            }

            $contextData = $app['contextData'];
            $inputData = $app['inputData'];
            $formData = $app['formData'];
            $trackingData = $app['trackingData'];
            $appData = $app['appData'];

            $contextData['session']['id'] = $app['session']->getId() ;

            if (null !== $app['session']->get('user')) {
                $appData['user'] = $app['session']->get('user');
                $contextData['session']['loginId'] = $appData['user']['loginId'];
            }

            $contextData['httpVerb'] = $app['request']->getMethod() ;
            $contextData['uri'] = $app['request']->getUri() ;
            $contextData['contentType'] = $app['request']->headers->get('Content-Type');
            $contextData['userAgent'] = $app['request']->headers->get('User-Agent');
            $contextData['clientIP'] = $app['request']->getClientIp();
            $contextData['referer'] = $app['request']->headers->get('referer');
            $contextData['b'] = $app['bcookie'];
            $contextData['languageAccepted'] = $app['request']->getLanguages();

            // Standard processing on either query string parameters, POST body parameters, or POST/PUT/DELETE json body
            if ( $contextData['httpVerb'] == 'GET' ) {
                $formData = $app['request']->query->all();
            } else {
                if (0 === strpos($contextData['contentType'], 'application/json')) {
                    $formData = json_decode($app['request']->getContent(), true);
                    $contextData['jsonBody'] = true;
                } else {
                    $formData = array_merge(
                        $app['request']->query->all(),
                        $app['request']->request->all());
                }
            }

            // Input data is just the data for Models
            // where form data contain both Model and controller data
            if ( !empty( $formData ) ) {
                $inputData = $formData;
                $removal = array('csrfToken','_page','_limit','_access_token','_csrf_token','submit', '_redirect', '_debug', '_locale', '_lang');

                if ( !empty($inputData) ) {
                    foreach ( $inputData as $key => $value )
                    {
                        if ( in_array($key, $removal) ) {
                            unset( $inputData[$key] ) ;
                        } else if ( preg_match("/^_/", $key) && strtolower($key) != '_id' ) {
                            unset( $inputData[$key] ) ;
                        }
                    }
                }
            }

            $app['contextData'] = $contextData;
            $app['inputData'] = $inputData;
            $app['formData'] = $formData;
            $app['trackingData'] = $trackingData;
            $app['appData'] = $appData;
        }

        if($app['request']->cookies->has("l")) {
            $app['lcookie'] = $app['request']->cookies->get('l');
        }
        $languageAccepted = $app['request']->getLanguages();

        if ( !empty($app['formData']['_lang']) ) {
            $app['language'] = $app['formData']['_lang'] ;
            $app['lcookie'] = $app['language'];
        } else if ( !empty($app['lcookie']) ) {
            $app['language'] = $app['lcookie'];
        } else if ( !empty($languageAccepted) ) {
            if ( strtolower($languageAccepted[0]) == 'zh-tw'
                    || strtolower($languageAccepted[0]) == 'zh-hk'
            ) {
                $app['language'] = 'zh-tw';
            } else if ( strtolower( substr($languageAccepted[0],0,2) ) == 'id' ) {
                $app['language'] = 'ms';
            } else {
                $app['language'] = substr( $languageAccepted[0],0,2);
            }
        } else {
            $app['language'] = 'en';
        }

        $app['monolog']->debug('['.$app['txid'].']App>'."Setting language to " . $app['language']);
        // Added mustache helper functions
        // these are available to all

        $app->register(new \Silex\Provider\TranslationServiceProvider(), array(
            'locale' => $app['language'],
            'locale_fallbacks' => array('en'),
        ));

        $app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
            $t1 = microtime(true);
            $localeData = apc_fetch('silexmonkey.core.translation.data');
            $locales = apc_fetch('silexmonkey.core.translation.locales');

            if ( empty($localeData) ) {
                $localeData = [];
                $locales = [];
                $yaml = new \Symfony\Component\Yaml\Parser();
                $ymlFileList = '';
                $ymls = scandir($app['app_path'].'/locales');

                foreach ( $ymls as $yml )
                {
                    if ( preg_match('/^\./', $yml ) ) {
                        continue;
                    }

                    if ( preg_match('/[a-zA-Z\-\_0-9]{2}\.yml$/', $yml) ) {
                        $ymlFileList .= $yml . ',';
                        $locale = explode(".", $yml)[0];
                        $locales[] = $locale;
                        $localeData[ $locale ] = $yaml->parse(file_get_contents($app['app_path'].'/locales/'.$yml));
                        apc_store('silexmonkey.core.translation.set.' . $locale , $localeData[$locale]['Language']);
                    }
                }

                apc_store('silexmonkey.core.translation.data', $localeData);
                apc_store('silexmonkey.core.translation.locales', $locales);
            }
            $t2 = microtime(true);
            $app['monolog']->addDebug('SilexMonkey\App.php>scanned and read translation yml files in ' . sprintf('%.6f', $t2-$t1) . 'sec, @' . $t2 );

            $translator->addLoader('array', new \Symfony\Component\Translation\Loader\ArrayLoader());
            foreach ( $locales as $thisLocale ) {
                $translator->addResource('array', $localeData[$thisLocale] , $thisLocale);
            }

            $t3 = microtime(true);
            $app['monolog']->addDebug('SilexMonkey\App.php>translator loaded in ' . sprintf('%.6f', $t3-$t2) . 'sec, @' . $t2 );

            return $translator;
        }));

        $app['mustache'] = $app->share($app->extend('mustache', function ($mustache, $app) {
                $mustache->addHelper('urlencode', function($text, $m){ return \SilexMonkey\Decorators\SEO\Text::formatForURL($m->render($text)); } );
                $mustache->addHelper('rawurlencode', function($text, $m){ return rawurlencode($m->render($text)); } );
                $mustache->addHelper('i18n', function($text, $m){ return gettext($m->render($text)); } );
                $mustache->addHelper('_t', function($text, $m) use($app) { return $m->render($app['translator']->trans($text)); } );
                $mustache->addHelper('_rt', function($text, $m) use($app) { return $app['translator']->trans($m->render($text)); } );
                $mustache->addHelper('shorten32', function($text, $m){ return substr($m->render($text), 0, 32); } );
                $mustache->addHelper('shorten64', function($text, $m){ return substr($m->render($text), 0, 64); } );
                $mustache->addHelper('shorten128', function($text, $m){ return substr($m->render($text), 0, 128); } );
                return $mustache;
        }));

        $app->error(function (\Exception $e, $code) use ($app) {
            $app['monolog']->debug('['.$app['txid'].']App>'." Caught Error, code =  " . $code );
            $app['monolog']->debug('['.$app['txid'].']App>'." Caught Error, message =  " . $e->getMessage() );

            $appData = $app['appData'] ;
            $trackingData = $app['trackingData'] ;
            if (
                !empty($app['config']['control']['trailTrack'])
                && $app['config']['control']['trailTrack']
                && $trackingData['controller'] != 'SilexMonkey\Controllers\TrackingAPIController::track'
                ) {
                $trackingData['httpCode'] = $code;
                $trackingData['context'] = $app['contextData'];
                if ( strlen($trackingData['context']['contentType']) < 1 ) {
                    unset($trackingData['context']['contentType']);
                }

                $trackingData['peakMem'] = round(memory_get_peak_usage()/(1024*1024),2) ;
                $trackingData['endTS'] = microtime(true) ;
                $trackingData['duration'] = $trackingData['endTS'] - $trackingData['startTS'];
                $trail = new \SilexMonkey\Models\Trail($app);
                $id = $trail->save($trackingData, ['_id' => $app['txid']]);
            }
            $app['monolog']->debug('['.$app['txid'].']App>'." Error handling completed ");

            if ( $appData['jsonOut'] ) {
                return $app->json(['error'=>'opps', 'errorCode' => $code], $code);
            } else {
                return new \Symfony\Component\HttpFoundation\Response('We are sorry, but something went terribly wrong. Code:' . $code);
            }
        });

        $app->before(function (\Symfony\Component\HttpFoundation\Request $request) use (&$app) {
            if (extension_loaded('newrelic')) {
                newrelic_set_appname($_SERVER['SERVER_NAME']);
                newrelic_name_transaction($app['request']->get('_controller'));
            }

            $app['monolog']->debug('['.$app['txid'].']App>'."AppName = " . $_SERVER['SERVER_NAME']);
            $app['monolog']->debug('['.$app['txid'].']App>'."Transaction = " . $app['request']->get('_controller'));

            $contextData = $app['contextData'];
            $inputData = $app['inputData'];
            $formData = $app['formData'];
            $trackingData = $app['trackingData'];
            $appData = $app['appData'];

            $contextData['language'] = $app['language'];
            // Mobile detection
            $mobileDetect = $app['session']->get('m');
            if ( empty($mobileDetect) ) {
                $mobileDetect = array();
                $detect = new \Mobile_Detect;
                $mobileDetect['isMobile'] = $detect->isMobile();
                $mobileDetect['isTablet'] = $detect->isTablet();
                $app['session']->set('m', $mobileDetect);
            }

            $contextData['isMobile'] = $mobileDetect['isMobile'];
            $contextData['isTablet'] = $mobileDetect['isTablet'];

            $trackingData['serverName'] = $_SERVER['SERVER_NAME'];
            $trackingData['serverIP'] = $_SERVER['SERVER_ADDR'];
            $trackingData['controller'] = $app['request']->get('_controller');
            $trackingData['_id'] = $app['txid'];

            $app['contextData'] = $contextData;
            $app['inputData'] = $inputData;
            $app['formData'] = $formData;
            $app['trackingData'] = $trackingData;
            $app['appData'] = $appData;
        });

        $app->after(function (\Symfony\Component\HttpFoundation\Request $request,\Symfony\Component\HttpFoundation\Response $response) use (&$app) {
            $appData = $app['appData'] ;
            $contextData = $app['contextData'] ;
            $trackingData = $app['trackingData'] ;
            $trackingData['generateTS'] = microtime(true) ;
            if ( $appData['htmlOut'] && !$appData['jsonOut'] && !empty($trackingData['_id']) ) {
                $newContent = $response->getContent() ;
                if ( !empty($app['config']['control']['trailTrack']) && $app['config']['control']['trailTrack'] ) {
                    if (  empty($app['config']['control']['trailTrack'])
                        || (!empty($app['config']['control']['trailTrackDomains']) && in_array($_SERVER['SERVER_NAME'], $app['config']['control']['trailTrackDomains']) )
                    ) {
                        $timePreTrail = microtime(true) ;
                        $newContent = str_ireplace('<head>',\SilexMonkey\Helpers\TrailTracking::getHeaderJS($trackingData['_id'].''),$newContent);
                        $newContent = str_ireplace('</body>',\SilexMonkey\Helpers\TrailTracking::getFooterJS($contextData['isMobile']||$contextData['isTablet']),$newContent);
                        $trackingData['injectTime'] = microtime(true) - $timePreTrail;
                    }
                }

                if ( !empty($app['config']['control']['HTMLMinification']) && $app['config']['control']['HTMLMinification'] ) {
                    $timePreMini = microtime(true) ;
                    $search = array(
                        '/\>[^\S ]+/s', //strip whitespaces after tags, except space
                        '/[^\S ]+\</s', //strip whitespaces before tags, except space
                        '/(\s)+/s'  // shorten multiple whitespace sequences
                    );
                    $replace = array( '>', '<', '\\1');
                    $newContent = preg_replace($search, $replace, $newContent);
                    $trackingData['miniTime'] = microtime(true) - $timePreMini;
                }

                $response->setContent($newContent);
            }

            //Adding CORS header;
            $response->headers->set('Access-Control-Allow-Origin', $app['config']['control']['corsOrigin']);

            $cookies = $request->cookies;
            if(!$cookies->has("b")) {
                //Set Unique browser instance cookie
                //(string $name, string $value = null, integer|string|DateTime $expire, string $path = '/', string $domain = null, Boolean $secure = false, Boolean $httpOnly = true)
                $cookie = new \Symfony\Component\HttpFoundation\Cookie("b", $app['bcookie'], time() + 315360000);
                $response->headers->setCookie($cookie);
            }

            if( !empty($app['lcookie']) ) {
                $cookie = new \Symfony\Component\HttpFoundation\Cookie("l", $app['lcookie'], time() + 315360000, '/', null, false, false);
                $response->headers->setCookie($cookie);
            }

            $trackingData['sentTS'] = microtime(true) ;
            $app['trackingData'] = $trackingData;
        });

        $app->finish(function (\Symfony\Component\HttpFoundation\Request $request,\Symfony\Component\HttpFoundation\Response $response) use (&$app) {
            if ( !empty($app['config']['control']['assetic']) && $app['config']['control']['assetic'] && !empty( $app['appData']['asset'] ) ) {
                $assets = $app['appData']['asset'];
                foreach ( $assets as $asset )
                {
                    $app['redis']->sAdd('assetic|asset', $asset);
                }
            }

            if ( !empty($app['config']['control']['assetic']) && $app['config']['control']['assetic'] && !empty( $app['appData']['assetGeneration'] ) ) {
                $assetsNeedGeneration = $app['appData']['assetGeneration'];
                foreach ( $assetsNeedGeneration as $asset )
                {
                    $pending = $app['redis']->sMembers('assetic|generation');

                    if ( ! in_array($asset, $pending) ) {
                        $app['redis']->sAdd('assetic|generation', $asset);
                        $queueMaster = $app['queue'];
                        $queueMaster->setQueue('default');
                        $queueMaster->addJob(
                            "Assetic\\Generation",
                            array(
                                "asset" => $asset,
                            )
                        );
                    }
                }
            }

            $trackingData = $app['trackingData'] ;
            if (
                !empty($app['config']['control']['trailTrack'])
                && $app['config']['control']['trailTrack']
                && $trackingData['controller'] != 'SilexMonkey\Controllers\TrackingAPIController::track'
                && !empty($app['config']['control']['trailTrackDomains'])
                && in_array($_SERVER['SERVER_NAME'], $app['config']['control']['trailTrackDomains'])
                ) {
                $trackingData['pageSize'] = strlen($response->getContent());
                $trackingData['httpCode'] = $response->getStatusCode();
                $trackingData['pageOutTime'] = $trackingData['sentTS'] - $trackingData['startTS'];
                $trackingData['context'] = $app['contextData'];
                if ( strlen($trackingData['context']['contentType']) < 1 ) {
                    unset($trackingData['context']['contentType']);
                }

                $trackingData['peakMem'] = round(memory_get_peak_usage()/(1024*1024),2) ;
                $trackingData['endTS'] = microtime(true) ;
                $trackingData['duration'] = $trackingData['endTS'] - $trackingData['startTS'];
                $trail = new \SilexMonkey\Models\Trail($app);
                $id = $trail->save($trackingData, ['_id' => $app['txid']]);
            }
        });

        //Override the Silex ControllerResolver to allow $app injection if the controller has a injectApp method
        $app['resolver'] = $app->share(function () use ($app) {
            return new \SilexMonkey\ControllerResolver($app, $app['logger']);
        });

        return $app;
    }
}
