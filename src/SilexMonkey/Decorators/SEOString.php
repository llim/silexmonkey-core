<?php

namespace SilexMonkey\Decorators;

class SEOString
{
    public static function shortenForDescription($string, $minLength = 60, $maxLength = 155, $preferDelimiter=".")
    {
        return self::shorten($string, $minLength, $maxLength, $preferDelimiter);
    }

    public static function shortenForTitle($string, $minLength = 20, $maxLength = 55, $preferDelimiter=".")
    {
        return self::shorten($string, $minLength, $maxLength, $preferDelimiter);
    }

    public static function shorten($string, $minLength, $maxLength, $preferDelimiter)
    {
        if ( strlen($string) < $maxLength ) {
            return $string;
        }

        if ( strlen($string) < $minLength ) {
            return $string;
        }

        $allDelimiterPosition = self::getAllPositionOfCharacter($preferDelimiter, $string);

        $bestPosition = -1;
        for($n=0;$n<count($allDelimiterPosition);$n++)
        {
            $thisPosition = $allDelimiterPosition[$n];
            if ( $thisPosition >= ($minLength-1) && $thisPosition <= ($maxLength-1) ) {
                if ( $thisPosition > $bestPosition ) {
                    $bestPosition = $thisPosition;
                }        
            }   
        }

        // can't find the delimiter in the comfort zone
        if ( $bestPosition == -1 ) {
            $returnString = substr($string, 0, $maxLength);     
            $lastSpacePosition = strrpos($returnString, ' ');

            if ( !$lastSpacePosition && $lastSpacePosition > $minLength ) {
                return substr($returnString, 0, $lastSpacePosition);
            } else {
                return $returnString;
            }
        } else {
            return substr($string, 0, $bestPosition);   
        }
    }

    public static function getAllPositionOfCharacter($needle, $haystack)
    {
        $stringSplitted = str_split($haystack);
        
        $pos = [];
        for($n=0; $n<count($stringSplitted); $n++)
        {
            if ( $stringSplitted[$n] == $needle ) {
                $pos[] = $n;
            } 
        }

        return $pos;
    }
}
