<?php

namespace SilexMonkey\Decorators\SEO;

class Text
{
    public static function formatForURL($string)
    {
        $string = strtolower($string);
        $string = str_replace("&amp;"," ",$string);
        $string = str_replace("&"," ",$string);
        $string = trim(preg_replace("/[^ a-z0-9]/", " ", $string));
        $string = str_replace(" ", "-", $string);
        $string = preg_replace("/\-+/", "-", $string);
        $string = urlencode($string);

        return $string;
    }
    
}
