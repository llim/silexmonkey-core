<?php

namespace SilexMonkey\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

class QueueProvider implements ServiceProviderInterface
{
    public function boot(Application $app)
    {
    }

    public function register(Application $app)
    {
        $app['queue'] = $app->share(function () use ($app) {
        	return new \SilexMonkey\Queue\QueueMaster(
                $app['queue.mongo.host'],
                $app['queue.mongo.port'],
                $app['queue.mongo.db'],
                $app['queue.redis.host'],
                $app['queue.redis.port'],
                $app['queue.name']
            );
        });
    }
}
