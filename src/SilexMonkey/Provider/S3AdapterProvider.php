<?php

namespace SilexMonkey\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class S3AdapterProvider implements ServiceProviderInterface
{
    public function boot(Application $app)
    {
    }

    public function register(Application $app)
    {
        $app['S3Flysystem'] = $app->share(function () use ($app) {
            $S3Bucket = (   
                !empty($app['config']['application']['mediaS3Bucket']) 
                ? $app['config']['application']['mediaS3Bucket'] 
                : ( !empty($app['S3Flysystem.S3Bucket']) ? $app['S3Flysystem.S3Bucket'] : null ) ) ;

            if ( empty($S3Bucket) ) {
                return null ;
            }

            $client = $app['aws']->get('s3');
            return new Filesystem(new Adapter($client, $S3Bucket));
        });
    }
}
