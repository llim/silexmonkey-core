<?php
namespace SilexMonkey;

//Include the namespaces of the components we plan to use
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Debug;
use Knp\Provider\ConsoleServiceProvider;

class Console
{
    public static function boot($appPath)
    {

        $app = require($appPath.'/app/app.php');
        $app->register(new ConsoleServiceProvider(), array(
            'console.name' => 'SilexMonkey Console',
            'console.version' => '1.0.0',
            'console.project_directory' => __DIR__ . '/..'
        ));
        
        //Instantiate our Console application
        $consoleService = $app['console'];

        //Default command provided by SilexMonkey
        $consoleService->add(new \SilexMonkey\Commands\Hello);
        $consoleService->add(new \SilexMonkey\Commands\CacheClear);
        $consoleService->add(new \SilexMonkey\Commands\WorkerStart);
        $consoleService->add(new \SilexMonkey\Commands\AddJob);
        $consoleService->add(new \SilexMonkey\Commands\QueueAge);
        $consoleService->add(new \SilexMonkey\Commands\QueueSize);
        $consoleService->add(new \SilexMonkey\Commands\RequeueJob);
        $consoleService->add(new \SilexMonkey\Commands\AsseticGeneration);

        $userJobNameSpace = $app['config']['console']['namespace'];
        $userJobDirectory = $appPath.'/app'.str_replace('\\', '/', $userJobNameSpace) ;
        $userCommands = scandir( $userJobDirectory );

        foreach ( $userCommands as $command )
        {
            if ( preg_match('/^\./', $command) ) {
                continue;
            }

            if ( preg_match('/\.php$/', $command) ) {
                $thisClass = $userJobNameSpace . "\\" . str_replace(".php", '',  $command);
                $consoleService->add(new $thisClass);
            }
        }

        return $consoleService;
    }
}
