<?php

namespace SilexMonkey\Models;

use SilexMonkey\Helpers\Validizor as Validizor;

abstract class BaseModel
{

    protected $app = null;
    protected $_data = array();
    protected $dataRules = array();
    protected $error = '';

    public function __construct(\Silex\Application &$app=null, $data = array())
    {
        if (!empty($app)){
            $this->app = $app;
        }

        if (!empty($data)) {
            $this->_data = $data;
        }
        $this->initBase($data);
        $this->initModel($data);
        $this->postInit();
    }

    protected function initBase($data)
    {
        /**
        * @codeCoverageIgnore
        */
    }

    protected function initModel($data)
    {
        if (!empty($data)) {
            foreach ( $data as $key => $value ) {
                /* if ($key === '_id') {
                    continue;
                } */
                $setProperty = 'set' . ucfirst($key);
                $this->{$setProperty}($value);
                $this->_data[ $key ] = $value;
            }
        }
    }

    protected function postInit()
    {
        /**
        * @codeCoverageIgnore
        */
    }

    public function detachApp()
    {
        $this->app = null;
    }

    public function attachApp(\Silex\Application &$app=null)
    {
        $this->app = $app;
    }

    public function validate($targets=null)
    {
        $data = empty($targets) ? $this->_data : $targets ;
        $data = Validizor::validize($data, $this->dataRules) ;
        return $data;
    }

    public function getAttribute($attribute)
    {
        return $this->{$attribute};
    }

    public function setAttribute($attribute,$value)
    {
        $this->{$attribute} = $value;
    }

    public function unsetAttribute($attribute)
    {
        unset($this->{$attribute});
    }

    public function __call($method, $arguments)
    {
        $prefix = strtolower(substr($method, 0, 3));
        $property = lcfirst(substr($method, 3));

        if (empty($prefix) || empty($property)) {
            return;
        }

        if ($prefix == "get" && isset($this->{$property})) {
            return $this->{$property};
        } else if ($prefix == "get" && isset($this->_data[$property])) {
            return $this->_data[$property];
        }

        if ($prefix == "set") {
            if ( !empty($arguments[0]) && !is_null($arguments[0]) ) {
                if ( property_exists($this, $property) ) {
                    $this->{$property} = $arguments[0];
                } else {
                    $this->_data[$property] = $arguments[0];
                }
            } else if ( is_null($arguments[0]) ) {
                if ( property_exists($this, $property) ) {
                    $this->{$property} = null;
                } else if ( !empty($this->_data[$property]) ) {
                    unset($this->_data[$property]);
                }
            }
        }
    }

    public function getError()
    {
        return $this->error;
    }

    public function toArray()
    {
        return $this->_data;
    }

    protected function addDebug($message)
    {
        $debugTrace = '';
        if ( !empty($this->app['config']['control']['logCallerInDebug'])
            && $this->app['config']['control']['logCallerInDebug'] ) {
            $trace = debug_backtrace(false);

            $debugTrace =
                ( !empty($this->app['txid']) ? '['.$this->app['txid'].']' : '' ) .
                ( !empty($trace[1]['class']) ? ($trace[1]['class'] . (!empty($trace[1]['type']) ? $trace[1]['type'] : '::')) : '' ) .
                ( !empty($trace[1]['function']) ? $trace[1]['function'] : '' ) .
                ( !empty($trace[1]['line']) ? '#'. $trace[1]['line'] : '' ) .
                '>';
        }

        $this->app['monolog']->addDebug( $debugTrace . $message );
    }

    abstract public function doesExist();
}
