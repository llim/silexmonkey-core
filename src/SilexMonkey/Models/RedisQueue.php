<?php

namespace SilexMonkey\Models;

class RedisQueue
{
    protected $redis = null;
    protected $queueName = null;
    protected $error = null;

    public function __construct(\Silex\Application &$app, $queueName=null)
    {
        $this->redis = $app['redis'];

        if ( !empty($queueName) ) {
            $this->queueName = $queueName;
        }
    }

    public function setQueue($queueName)
    {
        if ( !empty($queueName) ) {
            $this->queueName = $queueName;
            return true;
        }
        
        return false;
    }

    public function push($item)
    {
        if ( empty($this->queueName) ) {
            $this->error = "queue name not defined!";
            return false;
        }

        return $this->redis->rPush($this->queueName, $item);
    }

    public function pop()
    {
        if ( empty($this->queueName) ) {
            $this->error = "queue name not defined!";
            return null;
        }

        return $this->redis->lPop($this->queueName);
    }

    public function popAll()
    {
        $ret = $this->redis->multi()
                ->lRange($this->queueName, 0, -1)
                ->del($this->queueName)
                ->exec();

        return $ret[0];
    }

    public function clearAll()
    {
        return $this->redis->lTrim($this->queueName, 0, -1);
    }

    public function getError()
    {
        return $this->error;
    }
}
