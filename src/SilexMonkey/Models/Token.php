<?php

namespace SilexMonkey\Models;

class Token extends MongoModel
{
    protected $collection = "tokens";
    protected $_idMappedField = 'token';
    protected $useMongoId = false;

    protected $dataRules = array(
            "state" => array(
                array("type" => "inarray", "rule" => array(0,1,2))
            ),
            "type" => array(
                array("type" => "inarray", "rule" => array(0,1,2))
            ),
            "userId" => array(
                array("type" => "email")
            )
    );

    protected $required = array("token", "type", "state", "userId");

    public function activate()
    {
        return $this->update(array("state"=>0));
    }

    public function deactivate()
    {
        return $this->update(array("state"=>1));
    }

    public function getAll()
    {
        return $this->retrieve();
    }
}
