<?php

namespace SilexMonkey\Models;

class Trail extends \SilexMonkey\Models\MongoModel
{
    protected $collection = "trails";
    protected $required = [];
    protected $useMongoId = true;
}
