<?php

namespace SilexMonkey\Models;

class MediaModel extends \SilexMonkey\Models\MongoModel
{
    protected $collection = "media";
    protected $_idMappedField = 'mediaId';
    protected $useMongoId = true;

    protected $dataRules = array(
            "loginId" => array(
                    array("type" => "email"),
            ),
            "name" => array(
                    array("type" => "length", "rule" => 256),
                    array("type" => "htmlescape", "rule" => '')
            ),
            "originalName" => array(
                    array("type" => "length", "rule" => 512),
                    array("type" => "htmlescape", "rule" => '')
            ),
            "size" => array(
                    array("type" => "integer"),
            ),
            "md5" => array(
                    array("type" => "length", "rule" => 128),
            ),
            "status" => array(
                array("type" => "inarray", "rule" => array("ACTIVE","INACTIVE","LOCKED","DELETED"))
            ),

    );

    protected $required = array("name", "originalName", "extension", "size", "mime", "md5");

    public function loadSelfFromDB( $idArray = array() )
    {
        if ( empty($idArray) && !empty($this->_id) ) {
            $data = $this->retrieveOne(array("_id" => $this->_id));
        } else if ( empty($idArray) && !empty($this->_data['_id']) ) {
            $data = $this->retrieveOne(array("_id" => $this->_data['_id']));
        } else if ( !empty($idArray) && !empty($idArray['mediaId']) ) {
            $data = $this->retrieveOne(array("_id" => new \MongoId($idArray['mediaId']) ));
        } else {
            $data = $this->retrieveOne( $idArray );
        }
        $this->initModel($data);
    }

    public function delete($match=null)
    {
        return $this->update(array("status"=>"DELETED"));
    }


    public function getMultiUrl($sizes)
    {
        $storage = $this->getStorage();
        $arrayOfUrl = array();
        foreach ( $sizes as $type ) {
            switch ( $storage )
            {
                case 'S3':
                    $arrayOfUrl[$type] = $this->generateS3URL($type);
                    break;
                case 'local':
                default:
                    $arrayOfUrl[$type] = $this->generateLocalURL($type);
                    break;
            }
        }

        return $arrayOfUrl;
    }

    public function getUrl($type="ORIGINAL")
    {
        if ( empty($this->_data['storage']) ) {
            return null;
        }

        $storage = $this->_data['storage'];
        switch ( $storage )
        {
            case 'S3':
                return $this->generateS3URL($type);
            case 'local':
            default:
                return $this->generateLocalURL($type);
        }
    }

    public function generateLocalURL($type)
    {
        return "/images/vendor/" . $this->_data['name'] ;
    }

    public function generateS3URL($type, $noCDN=false)
    {
        $thisMediaId = $this->_id . "";
        $bucketHash = substr($thisMediaId, -1);
        if ( !empty($this->app['config']['media']['cdnEnabled'])
                && $this->app['config']['media']['cdnEnabled']
                && !empty( $this->app['config']['media']['cdnBaseDomain'] )
                && !$noCDN ) {
            return '//' . $this->app['config']['media']['cdnBaseDomain']
                . "/" . $bucketHash . "/" . $thisMediaId . "/" . $this->getMyFilenameForType($type) ;
        } else {
            return "//s3-ap-southeast-1.amazonaws.com/" . $this->app['config']['application']['mediaS3Bucket'] . "/" . $bucketHash . "/" . $thisMediaId . "/" . $this->getMyFilenameForType($type) ;
        }
    }

    public function getMyFilenameForType($type)
    {
        $thisMediaId = $this->_id . "";
        switch ( $type )
        {
            case 'ORIGINAL':
                return $thisMediaId . "."  . $this->_data['extension'];
            default:
                return $type . ".jpg";
        }
    }

    public function isS3Ready()
    {
        if ( !empty($this->_data['s3Ready']) && $this->_data['s3Ready'] ) {
            return true;
        } else {
            return false;
        }
    }
}
