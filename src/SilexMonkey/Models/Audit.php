<?php

namespace SilexMonkey\Models;

class Audit extends MongoModel
{
    protected $collection = "audits";
    protected $_idMappedField = 'id';
    protected $useMongoId = true;

    protected $dataRules = array();

    protected $required = array("app", "section", "action", "userId", "ip", "userAgent");

    protected function getAppName()
    {
        return $this->app['trackingData']['serverName'];
    }

    public function add($data=array())
    {
        $data['app'] = $this->getAppName();
        $data['userId'] = (!empty($this->app['contextData']['session']['loginId'])) ? $this->app['contextData']['session']['loginId']: null;
        $data['ip'] = $this->app['contextData']['clientIP'] ;
        $data['userAgent'] = $this->app['contextData']['userAgent'] ;
        return $this->create($data);
    }
}
