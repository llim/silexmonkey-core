<?php

namespace SilexMonkey\Models;

class RedisSet
{
    protected $redis = null;
    protected $setName = null;
    protected $error = null;

    public function __construct(\Silex\Application &$app, $setName=null)
    {
        $this->redis = $app['redis'];

        if ( !empty($setName) ) {
            $this->setName = $setName;
        }
    }

    public function setSetName($setName)
    {
        if ( !empty($setName) ) {
            $this->setName = $setName;
            return true;
        }
        
        return false;
    }

    public function add($item)
    {
        if ( empty($this->setName) ) {
            $this->error = "set name not defined!";
            return false;
        }

        return $this->redis->sAdd($this->setName, $item);
    }

    public function remove($item)
    {
        if ( empty($this->setName) ) {
            $this->error = "set name not defined!";
            return null;
        }

        return $this->redis->sRem($this->setName, $item);
    }

    public function getWholeSet()
    {
        return $this->redis->sMembers($this->setName);
    }

    public function getError()
    {
        return $this->error;
    }
}
