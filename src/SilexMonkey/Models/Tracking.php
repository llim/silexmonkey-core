<?php

namespace SilexMonkey\Models;

class Tracking extends MongoModel
{
    protected $collection = "trackings";
    protected $dataRules = array();
    protected $required = array();
}
