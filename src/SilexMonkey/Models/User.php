<?php

namespace SilexMonkey\Models;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class User extends MongoModel implements AdvancedUserInterface
{
    protected $collection = "users";

    /*
    private $username;
    private $service;
    private $uid;
    private $password;
    private $email;
    private $enabled;
    private $accountNonExpired;
    private $credentialsNonExpired;
    private $accountNonLocked;
    private $roles;
    */

    public function create($data=array())
    {
        if ( empty($data) ) {
            $data = $this->_data;
        }

        if ( empty($data) ) {
            return null;
        }

        if ( !empty($data['password']) ) {
            $data['password'] = $this->generatePasswordHash($data['password']);
        }

        if ( !empty($data['passwordConfirmation']) ) {
            unset($data['passwordConfirmation']);
        }

        if ( !empty($data['csrfToken']) ) {
            unset($data['csrfToken']);
        }

        return parent::create($data);
    }

    public function verifyPassword($password)
    {
        if ( !empty( $this->_data['password'] ) ) {
            return password_verify($password, $this->_data['password']);
        } else {
            return false;
        }
    }


    public function doesExist()
    {
        return $this->mongo
                    ->find(array("socialId.provider"=>$this->_data['socialId']['provider'], "socialId.id" => $this->_data['socialId']['id']))
                    ->count() > 0 ;
    }


    /**
     * Gets the user email.
     *
     * @return string
     */
    public function getEmail()
    {
        return ( !empty($this->_data['email']) ? $this->_data['email'] : null );
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return ( !empty($this->_data['roles']) ? $this->_data['roles'] : null );
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return ( !empty($this->_data['password']) ? $this->_data['password'] : null );
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return ( !empty($this->_data['username']) ? $this->_data['username'] : null );
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return $this->_data['userNonExpired'];
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return $this->_data['userNonLocked'];
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return $this->_data['credentialsNonExpired'];
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->_data['enabled'];
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    public function loadSelfFromDB( $idArray=array() )
    {
        if ( empty($idArray) ) {
            $data = $this->retrieveOne(array("service" => $this->_data['service'], "uid" => $this->_data['uid']));
        } else {
            $data = $this->retrieveOne($idArray);
        }
        $this->initModel($data);
    }

    public function toArray()
    {
        $returnArray = $this->_data;
        // Password should only be retrieved by getPassword() only
        unset( $returnArray['password'] );
        return $returnArray;
    }

    public function generatePasswordHash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}
