<?php

namespace SilexMonkey\Models;

class MongoModel extends BaseModel implements CRUDInterface
{
    protected $collection = 'dummy';
    protected $mongo = null;
    protected $id = null;
    protected $_id = null;
    protected $_idMappedField = null;
    protected $dbLoadedCount = 0;
    protected $useMongoId = true;
    protected $queryResultCount = -1;

    protected $required = array();
    protected $fieldMap = array();

    protected $timeMarker = [];

    protected function initBase($data)
    {
        $dbname = $this->app['config']['mongo']['dbname'];
        $this->addDebug("dbname = $dbname, Collection = " . $this->collection);
        $this->mongo = $this->app['mongo']['default']->selectCollection($dbname,$this->collection);
    }

    protected function initModel($data)
    {
        if ( !empty($data['_id']) ) {
            $data['_id'] = $this->useMongoId ? ( (is_a($data['_id'], "MongoId")) ? $data['_id'] : new \MongoId($data['_id']) ) : $data['_id'];
            $this->_id = $data['_id'];
        } else  if ( !empty($this->_idMappedField) && !empty($data[$this->_idMappedField]) ) {
            $this->addDebug("Mapping " . $this->_idMappedField . " to _id ");
            $data['_id'] = $this->useMongoId ? ( is_a( $data[$this->_idMappedField], "MongoId") ? $data[$this->_idMappedField] : new \MongoId($data[$this->_idMappedField]) ) : $data[$this->_idMappedField];
            $this->_id = $data['_id'];
        }

        if ( !empty( $this->_idMappedField ) && empty( $data[$this->_idMappedField] ) && !empty($data['_id']) ) {
            $this->addDebug("Mapping _id to " . $this->_idMappedField);
            $data[$this->_idMappedField] = $this->useMongoId ? ( is_a( $data['_id'], "MongoId") ? $data['_id'] : new \MongoId($data['_id']) ) : $data['_id'];
        }
        $this->addDebug("Mongo Object data init with _id " . print_r($this->_id, true) );

        parent::initModel($data);
    }

    protected function convertToArray(\MongoCursor $cursor)
    {
        if ( $cursor->count() > 0 ) {
            return iterator_to_array($cursor, false);
        } else {
            return array();
        }
    }

    public function create($data = null)
    {
        if (empty($data)) {
            if (empty($this->_data)) {
                return null;
            }
        } else {
            if ( !empty($this->_idMappedField) && !empty($data[ $this->_idMappedField ]) ) {
                $data['_id'] = $data[$this->_idMappedField] ;
            }
            $this->_data = $data;
            $this->postInit();
        }

        $this->_data = $this->validate();
        if ( \SilexMonkey\Helpers\Validizor::getNumberOfChange() > 0 ) {
            $changes = \SilexMonkey\Helpers\Validizor::getChanges();
            $this->addDebug("These are the field (" . count($changes) . ") values that validized:");
            foreach ( $changes as $field => $change ) {
                $thisMessage = "Field $field: ";
                foreach ( $change as $key => $value ) {
                    $thisMessage .= $key . "=>" . $value ;
                }
                $this->addDebug($thisMessage);
            }
        }

        if ( empty($this->_data['createdTS']) ) {
            $this->_data['createdTS'] = new \MongoDate();
        }

        if ( empty($this->_data['updatedTS']) ) {
            $this->_data['updatedTS'] = new \MongoDate();
        }

        $d = var_export( $this->_data, true );
        $this->addDebug("data = " . $d );

        try {
            $this->markTime('insert');
            $status = $this->mongo->insert($this->_data);
            $this->saveTime('insert');
            if (!empty($this->_data['_id'])) {
                $this->id = (string)$this->_data['_id'];
                if ( !empty($this->_idMappedField) ) {
                    $this->_data[$this->_idMappedField] = (string)$this->_data['_id'];
                }
                $this->postCreateAction($status);
                return $this->id;
            } else {
                $this->error = "Null _id detected after insertion";
                return null;
            }
        } catch (\MongoCursorException $exception) {
            $this->error = $exception->getMessage();
            return null;
        }
    }

    public function save($change, $match)
    {
        return $this->update($change, $match, true, false);
    }

    public function update($change, $match=null, $upsert=false, $multiple=false)
    {
        if ( !$upsert && !empty($change['_id']) ) {
            $this->addDebug("_id can't be changed, removed from change array");
            unset($change['_id']);
        }

        if ( !empty($change['createdTS']) ) {
            $this->addDebug("createdTS can't be changed, removed from change array");
            unset($change['createdTS']);
        }

        if ( empty($change) ) {
            $this->addDebug("empty change array");
            return null;
        }

        $change = $this->validate($change);
        if ( \SilexMonkey\Helpers\Validizor::getNumberOfChange() > 0 ) {
            $changes = \SilexMonkey\Helpers\Validizor::getChanges();
            $this->addDebug("These are the field (" . count($changes) . ") values that validized:");
            foreach ( $changes as $field => $thisChange ) {
                $thisMessage = "Field $field: ";
                foreach ( $thisChange as $key => $value ) {
                    $thisMessage .= $key . "=>" . $value ;
                }
                $this->addDebug($thisMessage);
            }
        }

        if ( empty($change['updatedTS']) ) {
            $change['updatedTS'] = new \MongoDate();
        }

        $options = array(
            'upsert' => $upsert,
            'multiple' => $multiple
        );

        return $this->commonUpdate('update', ['$set'=> $change ], $match, $options);
    }

    public function increment($field, $increment = 1, $match=null)
    {
        return $this->commonUpdate('incr', ['$inc'=> [$field => $increment]], $match, []);
    }

    public function addToSet($change, $match=null, $upsert=false)
    {
        return $this->commonUpdate('addToSet', ['$addToSet'=>$change], $match, ['upsert'=>$upsert]);
    }

    public function pull($change, $match=null)
    {
        return $this->commonUpdate('pull', ['$pull'=>$change], $match, []);
    }

    public function push($change, $match=null)
    {
        return $this->commonUpdate('push', ['$push'=>$change], $match, []);
    }

    public function unsetField($field, $match=null, $multi=false)
    {
        return $this->commonUpdate('unset', ['$unset'=>[$field=>true]], $match, ['multiple'=>$multi]);
    }

    public function commonUpdate($action, $change, $match=null, $options=[])
    {
        if ( empty($match) && !empty($this->_id) ) {
            $this->addDebug("Empty match array, using _id");
            $match = array("_id" => $this->_id);
        }

        if ( empty($change) ) {
            $this->addDebug("empty change array");
            return null;
        }

        $this->addDebug("Match array" . print_r($match, true));
        $this->addDebug("Change Array" . print_r($change, true));
        $this->markTime($action);
        $result = $this->mongo->update($match, $change, $options);
        $this->saveTime($action);
        $this->postUpdateAction($match, $change, $result);
        $this->loadSelfFromDB();
        return $result;
    }

    public function delete($match=null)
    {
        if ( empty($match) && !empty($this->_id) ) {
            $match = array("_id" => $this->_id);
            $this->addDebug("Empty match array, using _id, match array = " . print_r($match,true));
        } else if ( empty($match) && !empty($this->_idMappedField) && !empty($this->data[ $this->_idMappedField ])  ) {
            $match = array("_id" => $this->data[ $this->_idMappedField ]);
            $this->addDebug("Empty match array, using _id, match array = " . print_r($match,true));
        }

        $this->markTime('delete');
        $result = $this->mongo->remove($match);
        $this->saveTime('delete');
        $this->postDeleteAction($match, $result);
        return $result;
    }

    public function loadSelfFromDB( $idArray = array() )
    {
        if ( !empty( $this->_id ) ) {
            $this->addDebug("_id use to load data is " . print_r($this->_id, true));
            $data = $this->retrieveOne(array("_id" =>  ( $this->useMongoId ) ?  new \MongoId( $this->_id) : $this->_id ));
            $this->initModel($data);
            return true;
        } else if ( !empty( $idArray )  ) {
            if ( !empty($this->_idMappedField) && !empty( $idArray[ $this->_idMappedField ] ) ) {
                $idArray['_id'] = ( $this->useMongoId ) ?  new \MongoId( $idArray[ $this->_idMappedField ] ) : $idArray[ $this->_idMappedField ] ;
                if ( $this->_idMappedField != '_id' ) {
                    unset( $idArray[ $this->_idMappedField ] );
                }
            } else if ( !empty( $idArray['_id'] ) ) {
                $idArray['_id'] = ( $this->useMongoId ) ?  new \MongoId( $idArray['_id'] ) : $idArray['_id'] ;
            }

            $this->addDebug("IdArray use to load data is " . print_r($idArray, true));

            $data = $this->retrieveOne( $idArray );
            $this->initModel($data);
            return true;
        } else {
            return false;
        }
    }

    public function retrieve($query=array(),$offset=-1, $limit=-1, $sort=array())
    {
        $trackingData = $this->app['trackingData'];
        $this->markTime('retrieve');
        $results = $this->mongo->find($query);
        $this->queryResultCount = $results->count();
        if ( $offset > 0 ) {
            if ( $limit > 0 ) {
                $results->skip($offset*$limit);
            } else {
                $results->skip($offset);
            }
        }

        if ( $limit > 0 ) {
            $results->limit($limit);
        }

        if ( !empty($sort) ) {
            $results->sort($sort);
        }
        $this->saveTime('retrieve');

        $this->markTime('processing');
        $results = $this->convertToArray($results);
        for ( $n=0; $n<count($results); $n++) {
            if ( !empty($this->_idMappedField) ) {
                $results[$n][ $this->_idMappedField ] = (string)$results[$n]['_id'];
            }

            if ( !empty($results[$n]['createdTS']) ) {
                $results[$n]['createdDate'] = date('Y-M-d h:i:s', $results[$n]['createdTS']->sec);
            }

            if ( !empty($results[$n]['updatedTS']) ) {
                $results[$n]['updatedDate'] = date('Y-M-d h:i:s', $results[$n]['updatedTS']->sec);
            }
        }
        $this->saveTime('processing');
        $this->postRetrieveAction($query, $offset, $limit, $sort, $results);

        return $results;
    }

    public function count($query=array())
    {
        return $this->mongo->count($query);
    }

    public function retrieveOne($query)
    {
        return $this->mongo->findOne($query);
    }

    public function findById(\MongoId $id)
    {
        return $this->mongo->findOne(array('_id'=>$id));
    }

    public function doesExist()
    {
        return $this->count(array("_id" => $this->_id)) > 0;
    }

    public function allRequiredExists()
    {
        $missingRequired = array();
        foreach ( $this->required as $requiredField )
        {
            if ( !isset( $this->_data[ $requiredField ] ) ) {
                array_push($missingRequired, $requiredField);
            }
        }

        if ( count($missingRequired) > 0 ) {
            $this->error = "Missing required field (" . implode(",", $missingRequired) . ")";
            return false;
        }
        return true;
    }

    protected function convertFieldName()
    {
        foreach ( $this->fieldMap as $fieldName => $newFieldName )
        {
            if ( !empty($this->_data[$fieldName]) ) {
                $this->_data[$newFieldName] = $this->_data[$fieldName];
                unset( $this->_data[$fieldName] );
            }
        }
    }

    protected function reverseFieldConversion()
    {
        $reverseFieldMap = array_flip( $this->fieldMap );
        foreach ( $reverseFieldMap as $fieldName => $newFieldName )
        {
            if ( !empty($this->_data[$fieldName]) ) {
                $this->_data[$newFieldName] = $this->_data[$fieldName];
                unset( $this->_data[$fieldName] );
            }
        }
    }

    public function findMax($field)
    {
        return $this->mongo->find()->sort(array($field => -1 ))->limit(1)->getNext();
    }

    public function getResultCount()
    {
        return $this->queryResultCount;
    }

    public function getMongoInstance()
    {
        return $this->mongo;
    }

    public function batchUpdate($change, $match)
    {
        if ( !empty($change['_id']) ) {
            $this->addDebug("_id can't be changed, removed from change array");
            unset($change['_id']);
        }

        $this->addDebug("Final match array:" . print_r($match, true));
        if ( !empty($change['createdTS']) ) {
            $this->addDebug("createdTS can't be changed, removed from change array");
            unset($change['createdTS']);
        }

        if ( empty($change) ) {
            $this->addDebug("empty change array");
            return null;
        }

        $change = $this->validate($change);
        if ( \SilexMonkey\Helpers\Validizor::getNumberOfChange() > 0 ) {
            $changes = \SilexMonkey\Helpers\Validizor::getChanges();
            $this->addDebug("These are the field (" . count($changes) . ") values that validized:");
            foreach ( $changes as $field => $thisChange ) {
                $thisMessage = "Field $field: ";
                foreach ( $thisChange as $key => $value ) {
                    $thisMessage .= $key . "=>" . $value ;
                }
                $this->addDebug($thisMessage);
            }
        }

        if ( empty($change['updatedTS']) ) {
            $change['updatedTS'] = new \MongoDate();
        }

        $this->addDebug("Match array" . print_r($match, true));
        $this->addDebug("Change array" . print_r($change, true));
        $options = [ 'multiple' => true ];

        $this->markTime('batch');
        $result = $this->mongo->update($match, array('$set'=>$change), $options);
        $this->saveTime('batch');

        return $result;
    }

    public function aggregate($pipes)
    {
        $this->markTime('aggregate');
        $result = $this->mongo->aggregate($pipes);
        $this->saveTime('aggregate');

        if ( $result['ok'] == 0 ) {
            $this->error = $result['code'] . " :: " . $result['errmsg'] ;
            return [];
        } else {
            return $result['result'];
        }
    }

    protected function markTime($action)
    {
        $this->timeMarker[$action] = microtime(true);
    }

    protected function saveTime($action, $type='mongo')
    {
        $trackingData = $this->app['trackingData'];
        if ( empty($trackingData[$type]) ) {
            $trackingData[$type] = [];
        }
        if ( empty($trackingData[$type][$this->collection]) ) {
            $trackingData[$type][$this->collection] = [];
        }
        if ( empty($trackingData[$type][$this->collection][$action]) ) {
            $trackingData[$type][$this->collection][$action] = [];
        }
        $trackingData[$type][$this->collection][$action][] = microtime(true) - $this->timeMarker[$action];
        $this->app['trackingData'] = array_merge($this->app['trackingData'], $trackingData);
    }

    protected function postCreateAction($result) {}
    protected function postUpdateAction($match,$change,$result) {}
    protected function postDeleteAction($match,$result) {}
    protected function postRetrieveAction($query, $offset, $limit,$sort,$result) {}
}
