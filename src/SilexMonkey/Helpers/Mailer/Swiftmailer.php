<?php

namespace SilexMonkey\Helpers\Mailer;

class Swiftmailer 
{
    private static $error = "";

    public static function sendMail($sendgrid, $from, $to, $subject, $message, $replyto=null)
    {
        $email    = new \SendGrid\Email();
        $email->addTo($to)->
                setFrom($from)->
                setSubject($subject)->
                setHTML($message)->
                setText(strip_tags($message));

        if ( !empty($replyto) ) {
            $email->setReplyTo($replyto);
        }

        $sendStatus = $sendgrid->send($email);

        $error = null;
        if ( !empty( $sendStatus->errors[0] ) ) {
            self::$error = $sendStatus->errors[0];
            return false;
        }

        return true;
    }

    public static function getLastError()
    {
        return self::$error;
    }
}
