<?php

namespace SilexMonkey\Helpers;

class OAuthHelper
{

    private $oAuthProvider;
    protected $app;
    public static $error = "";

    public function __construct( &$app )
    {
        $this->app = $app;
        try {
            $this->oAuthProvider = new \OAuthProvider();
            //$this->oAuthProvider->consumerHandler(array($this,'validateConsumerKey'));	
            //$this->oAuthProvider->timestampNonceHandler(array($this,'timestampNonceChecker'));
            //$this->oAuthProvider->tokenHandler(array($this,'tokenHandler'));
            //$this->oAuthProvider->setRequestTokenPath('/v1/oauth/request');  // No token needed for this end point
            //$this->oAuthProvider->checkOAuthRequest();
        } catch (OAuthException $E) {
            echo OAuthProvider::reportProblem($E);
            $this->oauth_error = true;
        }
    }

    public function validateConsumerKey( )
    {
        $thisConsumer = new \SilexMonkey\Models\User($this->app);
        $data = $thisConsumer->retrieve( array("consumerKey" => $this->oAuthProvider->consumer_key) );
        $thisConsumerKey = $data['consumerKey'];
        $thisConsumerKeyStatus = $data['keyStatus'];
        if ( $this->oAuthProvider->consumer_key != $thisConsumerKey ) {
            return OAUTH_CONSUMER_KEY_UNKNOWN;
        } else if ( $thisConsumerKeyStatus != 0) {
            return OAUTH_CONSUMER_KEY_REFUSED;
        }
        
        return OAUTH_OK;
    }

    public function validateNonce()
    {
        if ($this->oAuthProvider->nonce === 'bad') {
            return OAUTH_BAD_NONCE;
        } elseif ($this->oAuthProvider->timestamp == '0') {
            return OAUTH_BAD_TIMESTAMP;
        }
    
        return OAUTH_OK;
    }

    public function validateToken()
    {
        $thisToken = new \SilexMonkey\Models\Token($this->app, array("token" => $this->oAuthProvider->token));
        $thisToken->loadSelfFromDB();

        if( !$thisToken->doesExist() ) {
            return OAUTH_TOKEN_REJECTED;
        } else if($thisToken->getType()==1 && $this->token->getState()==1) {
            return OAUTH_TOKEN_REVOKED;
        } else if($thisToken->getType()==0 && $this->token->getState()==2) {
            return OAUTH_TOKEN_USED;
        } else if($thisToken->getType()==0 && $this->token->getVerifier() != $this->oAuthProvider->verifier) {
            return OAUTH_VERIFIER_INVALID;
        }

        $this->oAuthProvider->token_secret = $this->token->getSecret();
        return OAUTH_OK;
    }

    public static function checkAccess($thisToken, $userId=null, $appId=null)
    {
        if(!$thisToken->doesExist() && $thisToken->getType()!=1) {
            self::$error = 'invalid access token';
            return false;
        } else if($thisToken->getType()==1 && $thisToken->getState()==1) {
            self::$error='access token revoked';
            return false;
        } else if($thisToken->getType()==0) {
            self::$error='invalid token type';
            return false;
        }

        $expiration = $thisToken->getExpiration() ;
        if ( !empty($expiration) && $expiration < time() ) {
            self::$error='token expired';
            return false;
        }

        /*
        if ( !empty($userId) ) {
            $thisUser = new \SilexMonkey\Models\User($this->app, array("uid" => $userId));
            $thisUser->loadSelfFromDB();
            if( !$thisUser->doesExist() || $thisUser->isAccountNonLocked() ) {
                self::$error='no such user';
                return false;
            }
        }
        */

        // TBD: implement application ID check later for OAuth Token
        /*$thisApplication = new \SilexMonkey\Models\Application($this->app, array("appId" => $appId));
        $thisApplication->loadSelfFromDB();
        if(!$thisApp->doesExist()) {
            $this->error='no such app';
            return false;
        }*/

        return true;
    }

    public static function generateToken()
    {
        $thisOAuthProvider = new \OAuthProvider();
        return bin2hex($thisOAuthProvider->generateToken(32));
    }
}
