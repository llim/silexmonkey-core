<?php

namespace SilexMonkey\Helpers;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class S3
{
    public static function copyToS3($app, $localFilePath, $destination, $options = [])
    {
        $S3Bucket = (empty($options['s3bucket']) ? $app['config']['application']['mediaS3Bucket'] : $options['s3bucket']);
        $client = $app['aws']->get('s3');
        $flysystem = new Filesystem(new Adapter($client, $S3Bucket));
        $stream = fopen($localFilePath, 'r+');
        $path = pathinfo($localFilePath);

        $S3Options = [];
        $S3Options['visibility'] = (empty($options['visibility'])? 'public' : $options['visibility']);
        $S3Options['mimetype'] = (empty($options['mimetype'])?  \SilexMonkey\Helpers\MIMEType::getType($path['extension']) : $options['mimetype']);
        $S3Options['Expires'] = gmdate("D, d M Y H:i:s T", strtotime(empty($options['expires'])? '+3 years' : $options['expires']) );
        $S3Options['Cache-Control'] = 'public,max-age=' . (empty($options['maxAge']) ? 93312000 : $options['maxAge']) ;
        $S3Options['Metadata'] = ['Cache-Control' => $S3Options['Cache-Control'] ] ;

        if ( !empty($options['s3rr']) && $options['s3rr'] ) {
            $S3Options['StorageClass'] = 'REDUCED_REDUNDANCY';
        }

        if ( $flysystem->has($destination) ) {
            $copySuccess = $flysystem->putStream($destination, $stream, $S3Options);
        } else {
            $copySuccess = $flysystem->writeStream($destination, $stream, $S3Options);
        }

        if ( $copySuccess ) {
            return $flysystem->has($destination);
        }

        return false;
    }

}
