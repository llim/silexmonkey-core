<?php

namespace SilexMonkey\Helpers\ElasticSearch\SuggestionHelpers;

/**
 * Class Completion 
 * @package Elastica\Suggest
 * @link http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-suggesters-term.html
 */
class Completion extends \Elastica\Suggest\AbstractSuggest
{
    /**
     * @param int $fuzzy
     * @return \Elastica\Suggest\Completion
     */
    public function setFuzzy($fuzzy)
    {
        return $this->setParam("fuzzy", array("fuzziness" => $fuzzy));
    }
}
