<?php

namespace SilexMonkey\Helpers\ElasticSearch\QueryBuilder;

use \SilexMonkey\Helpers\ElasticSearch\FilterFactory;
use \Elastica\Filter\Term;
use \Elastica\Filter\Ids;
use \Elastica\Filter\BoolAnd;
use \Elastica\Filter\BoolOr;
use \Elastica\Filter\BoolNot;

class BaseQueryBuilder
{
	protected $queryString = null;
	protected $query = null;
	protected $operator = 'OR';
	protected $limit = 20;
	protected $offset = 0;
	protected $filters = array();

	public function __construct()
	{
		$this->query = new \Elastica\Query();
	}

	public function build($queryParameters)
	{
        if ( !empty($queryParameters['search']) ) {
		    $this->queryString = new \Elastica\Query\QueryString();
		    //'And' or 'Or' default : 'Or'
		    $this->queryString->setDefaultOperator($this->operator);
		    $this->queryString->setQuery($queryParameters['search']);

            if ( !empty($queryParameters['fields']) ) {
		        $this->queryString->setFields($queryParameters['fields']);
            }
            if ( !empty($queryParameters['default_field']) ) {
		        $this->queryString->setDefaultField($queryParameters['default_field']);
            }

		    $this->query->setQuery($this->queryString);
        }

		$this->query->setLimit($this->limit);
		$this->query->setFrom($this->offset);
	}

	public function getQuery()
	{
		return $this->query;
	}

	public function setLimit($limit)
	{
		$this->query->setLimit($limit);
	}

	public function setOffset($offset)
	{
		$this->query->setFrom($offset);
	}

	public function setFrom($offset)
	{
		$this->query->setFrom($offset);
	}

	public function setFields($fields)
	{
		$this->query->setFields($fields);
	}

	public function setSort($sorting)
	{
		$this->query->setSort($sorting);
	}

    public function setAndFilters()
    {

        if ( count($this->filters) == 1 ) {
            $this->query->setFilter($this->filters[0]);
        } else if ( count($this->filters) < 1 ) {
            return;
        }

        $esFilterAnd = new BoolAnd();
        foreach ( $this->filters as $filter ) {
            $esFilterAnd->addFilter( $filter );
        }

        $this->query->setFilter($esFilterAnd);
    }

    public function addFilter($filter)
    {
        $this->filters[] = $filter;
    }

    public function createTermFilter($field, $value)
    {
        if ( is_array($value) ) {
            $esFilterOr = new BoolOr();
            foreach ($value as $v)
            {
		        $thisFilter = FilterFactory::getFilter('term', $field, $v);
                $esFilterOr->addFilter($thisFilter);
            }
            $this->filters[] = $esFilterOr;
        } else {
		    $this->filters[] = FilterFactory::getFilter('term', $field, $value);
        }
    }

    public function createIdsFilter($type, $ids)
    {
		return FilterFactory::getFilter('ids', $type, $ids);
    }

    public function addFacet($facet)
    {
        $this->query->addFacet($facet);
    }


}
