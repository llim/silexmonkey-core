<?php

namespace SilexMonkey\Helpers\ElasticSearch;

use \Elastica\Filter\Term;
use \Elastica\Filter\Ids;
use \Elastica\Filter\Range;

class FilterFactory
{
    public static function getFilter($type, $field, $value)
    {
        switch($type)
        {
            case 'term':
                $thisFilter = new Term();
                $thisFilter->setTerm($field,$value);
                return $thisFilter;

            case 'ids':
                $thisFilter = new Ids();
                // Ids filter is apply per type
                $thisFilter->setType($field);
                $thisFilter->setIds($value);
                return $thisFilter;

            case 'daterange':
                $thisFilter = new Range();

                if ( empty($value['from']) ) {
                    $value['from'] = Date('c');
                }
                $condition = array('from' =>  $value['from']);

                if ( !empty($value['to']) ) {
                    $condition['to'] =  $value['to'];
                }
                $thisFilter->addField($field, $condition);
                return $thisFilter;

            case 'script':
                $thisFilter = new \Elastica\Fitler\Script($value);
                return $thisFilter;
            default:
                return null;
        }
    }
}
