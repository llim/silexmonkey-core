<?php

namespace SilexMonkey\Helpers\ElasticSearch\FacetHelpers;

use \Elastica\Filter\BoolAnd;
use \Elastica\Filter\BoolOr;

class BaseFacetHelper
{

    protected $facet = null; 

    public function setSize($size)
    {
        $this->facet->setSize($size);    
    }

    public function getFacet()
    {
        return $this->facet;
    }

    public function setFilter($filters, $operator = 'AND')
    {
        if ( count($filters) > 1 ) { 
            switch ( $operator )
            {
                case 'OR':
                    $esFilterOperator = new BoolOr();
                    break;
                case 'AND':
                default:
                    $esFilterOperator = new BoolAnd();
                break;
            }
     
            foreach ( $filters as $filter ) {
                $esFilterOperator->addFilter( $filter );    
            }
        } else {
            $esFilterOperator = $filters[0];
        }

        $this->facet->setFilter($esFilterOperator);
    }
}
