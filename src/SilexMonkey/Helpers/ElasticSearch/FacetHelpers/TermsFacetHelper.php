<?php

namespace SilexMonkey\Helpers\ElasticSearch\FacetHelpers;

class TermsFacetHelper extends BaseFacetHelper
{
    public function __construct($facetName = 'thisFacet')
    {
        $this->facet = new \Elastica\Facet\Terms($facetName);
    }

    public function setField($field)
    {
        $this->facet->setField($field);
    }

    public function setOrder($orderType)
    {
        //count, term, reverse_count, reverse_term
        // default is count
        $acceptableOrder = array('count','term','reverse_count','reverse_term');

        if ( in_array($orderType, $acceptableOrder) ) {
            $this->facet->setOrder( $orderType );    
        }
    }
}
