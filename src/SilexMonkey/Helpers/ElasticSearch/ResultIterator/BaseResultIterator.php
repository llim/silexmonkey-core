<?php

namespace SilexMonkey\Helpers\ElasticSearch\ResultIterator;

class BaseResultIterator
{
	protected $resultSet;

	public function __constructor(\Elastica\ResultSet $result)
	{
		$this->resultSet = $result;
	}

	public function getTotalHits()
	{
		return $this->resultSet->getTotalHits();
	}

	public function getArray()
	{
		$return = array();
		foreach($this->resultSet->getResults() as $thisResult) {
			$return[] = $thisResult->getData();
		}	
		return $return;
	}

    public function getFacetResult()
    {
        $result = $this->resultSet->getFacets();
    }
}
