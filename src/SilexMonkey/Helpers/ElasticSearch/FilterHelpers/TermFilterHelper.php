<?php

namespace SilexMonkey\Helpers\ElasticSearch\FilterHelpers;

class TermFilterHelper extends BaseFilterHelper
{
	public function __construct($field,$term)
	{
		$this->filter = new Term();
		$this->filter->setTerm($field,$term);
	}
}
