<?php

namespace SilexMonkey\Helpers\ElasticSearch\FilterHelpers;

use \Elastica\Filter\Term;

class BaseFilterHelper
{

    private $filter = null;

    public function getFilter()
    {
        return $this->filter;
    }
}
