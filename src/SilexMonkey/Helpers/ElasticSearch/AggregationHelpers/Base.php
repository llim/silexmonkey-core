<?php

namespace SilexMonkey\Helpers\ElasticSearch\AggregationHelpers;

use \Elastica\Filter\BoolAnd;
use \Elastica\Filter\BoolOr;

class Base
{

    protected $aggregation = null; 
    protected $name = 'thisAggregation'; 

    public function setSize($size)
    {
        $this->aggregation->setSize($size);    
    }

    public function getAggregation()
    {
        return $this->aggregation;
    }

    public function getResult( $aggregationName = null )
    {
        return $this->aggregation->getAggregation( (empty( $aggregationName ) ? $this->name : $aggregationName  ) );
    }

    public function setFilter($filters, $operator = 'AND')
    {
        switch ( $operator )
        {
            case 'OR':
                $esFilterOperator = new BoolOr();
                break;
            case 'AND':
            default:
                $esFilterOperator = new BoolAnd();
            break;
        }
    
        foreach ( $filters as $filter ) {
            $esFilterOperator->addFilter( $filter );    
        }

        $this->aggregation->setFilter($esFilterOperator);
    }
}
