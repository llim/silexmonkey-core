<?php

namespace SilexMonkey\Helpers\ElasticSearch\AggregationHelpers;

class Terms extends Base
{
    public function __construct($aggregationName = 'thisAggregation')
    {
        $this->name = $aggregationName ;
        $this->aggregation = new \Elastica\Aggregation\Terms($aggregationName);
    }

    public function setField($field)
    {
        $this->aggregation->setField($field);
    }

    public function setOrder($orderType)
    {
        //count, term, reverse_count, reverse_term
        // default is count
        $acceptableOrder = array('count','term','reverse_count','reverse_term');

        if ( in_array($orderType, $acceptableOrder) ) {
            $this->aggregation->setOrder( $orderType );    
        }
    }
}
