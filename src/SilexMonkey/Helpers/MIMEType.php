<?php

namespace SilexMonkey\Helpers;

class MIMEType
{
    public static function getType($extension)
    {
        switch(strtolower($extension)) 
        {
            case "png":
                return 'image/png';
            case "gif":
                return 'image/gif';
            case 'jpg':
            case 'jpeg':
                return 'image/jpeg';
            default:
                return 'image/jpeg';
        }
    }
}
