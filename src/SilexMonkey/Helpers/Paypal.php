<?php

namespace SilexMonkey\Helpers;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

class Paypal
{
    private $app = null;
    private $sdkConfig = array();
    private $currency = 'SGD';
    private $baseReturnUrl = 'http://vendor.sweetspringmedia.sg/order/payment/callback';

    public function __construct(\Silex\Application &$app)
    {
        $this->app = $app;
        $this->sdkConfig = array(
                'mode' => $app['config']['paypal']['mode'],
                'http.ConnectionTimeOut' => $app['config']['paypal']['timeout'],
                'log.LogEnabled' => $app['config']['paypal']['logging'],
                'log.FileName' => $app['app_path'] . '/logs/PayPal.log',
                'log.LogLevel' => $app['config']['paypal']['logLevel']
            );
        $this->currency = $app['config']['paypal']['currency'];
    }

    private function getApiContext()
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential($this->app['config']['paypal']['clientId'], $this->app['config']['paypal']['secret'], $this->sdkConfig),
            'Request' . time()
        );
                             
        $apiContext->setConfig($this->sdkConfig);
    
        return $apiContext;
    }

    public function getPaypalPaymentApprovalUrl($orderInfo)
    {
        $payer = new Payer();
        $payer->setPayment_method("paypal");
        
        $amount = new Amount();
        $amount->setCurrency($this->currency)
               ->setTotal($orderInfo['amount']);
        
        $transaction = new Transaction();
        $transaction->setDescription("Payment")
                    ->setAmount($amount);

        if ( !empty($orderInfo['itemList']) ) {
            $items = array();
    
            foreach ($orderInfo['itemList'] as $item) {
                $thisItem = new Item();
                $thisItem->setName( $item['name'] )
	                ->setCurrency($this->currency)
	                ->setQuantity($item['quantity'])
	                ->setPrice($item['price']);
                $items[] = $thisItem;
            }

            $itemList = new ItemList();
            $itemList->setItems( $items );
            $transaction->setItemList($itemList);
        }

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturn_url($this->baseReturnUrl . "/" . $orderInfo['id'] . "?success=true")
                     ->setCancel_url($this->baseReturnUrl . "/" . $orderInfo['id'] . "?cancel=true");
        
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirect_urls($redirectUrls)
                ->setTransactions(array($transaction));
        
        $apiContext = $this->getApiContext();

        try {
            $payment->create($apiContext);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            $this->app['monolog']->addError('Paypal Processing error' . $ex->getMessage() );
            return null;
        }

        $redirectUrl = $this->getRedirectUrl($payment);
        $paymentId = $payment->getId();

        return array($paymentId, $redirectUrl);
    }

    private function getRedirectUrl($payment)
    {
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                return $link->getHref();
            }
        }
        return null;
    }

    public function executePayment($paymentId, $payerId)
    {
        $apiContext = $this->getApiContext();
        $payment = Payment::get($paymentId, $apiContext);
        
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        
        return $payment->execute($execution, $apiContext);
    }
}
