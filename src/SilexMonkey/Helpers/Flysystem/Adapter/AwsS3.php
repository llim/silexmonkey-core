<?php

namespace SilexMonkey\Helpers\Flysystem\Adapter;

use Aws\S3\S3Client;
use Aws\S3\Enum\Group;
use Aws\S3\Enum\Permission;
use League\Flysystem\AdapterInterface;
use League\Flysystem\Config;
use League\Flysystem\Util;

class AwsS3 extends \League\Flysystem\AwsS3v2\AwsS3Adapter 
{
    /**
     * @var  array  $metaOptions
     */
    protected static $metaOptions = array(
        'Cache-Control',
        'Expires',
        'StorageClass',
        'ServerSideEncryption',
    );

}
