<?php

namespace SilexMonkey\Helpers;

use Assetic\Factory\AssetFactory;
use Assetic\AssetManager;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;
use Assetic\FilterManager;
use Assetic\Filter\Sass\SassFilter;
use Assetic\Filter\Yui;
use Assetic\AssetWriter;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class Assetic
{
    public static function GetCompiledCss(&$app, $cssFiles)
    {
        if ( empty($cssFiles) ) {
            return [];
        }
    
        return self::GetFilePath('css', $app, $cssFiles);
    }


    public static function GetCompiledJs(&$app, $jsFiles)
    {
        if ( empty($jsFiles) ) {
            return [];
        }
        return self::GetFilePath('js', $app, $jsFiles);
    }

    public static function GetFilePath($type, &$app, $files)
    {
        $thisCollection = implode(",", $files);
        $thisCollectionKey = md5($thisCollection);   

        list($cacheVersion,$collectionVersion) =    
                    $app['redis']->multi()
                            ->get('assetic|version')
                            ->get('assetic|'.$thisCollectionKey)
                            ->exec();
        if ( empty( $cacheVersion ) ) {
            $appData = $app['appData'];
            $appData['asset'][] = $type . "|" . $thisCollection;
            $app['appData'] = array_merge($app['appData'], $appData);

            return $files;
        } else if ( empty($collectionVersion) ) {
            $appData = $app['appData'];
            $appData['asset'][] = $type . "|" . $thisCollection;
            $appData['assetGeneration'][] = $type . "|" . $thisCollection;
            $app['appData'] = array_merge($app['appData'], $appData);

            return $files;
        } else if ( !empty($collectionVersion) && $collectionVersion == $cacheVersion ) {
            $filename = self::GenerateFilename($type, $thisCollectionKey, $cacheVersion) ;
            $url = self::GenerateWebPath($app, $type, $filename) ;
            return [ $url ];
        } else {
            return $files;
        }
    }    

    public static function Compile($app)
    {
        $thisVersion = dechex( time() );
        $collections = $app['redis']->sMembers('assetic|asset');

        $client = $app['aws']->get('s3');
        $S3Bucket = $app['config']['application']['mediaS3Bucket'];
        $filesystem = new Filesystem(new Adapter($client, $S3Bucket));

        foreach ( $collections as $thisCollection )
        {
            list($type, $collectionSet) = explode("|", $thisCollection);
            self::GenerateAsset($app, $thisVersion, $filesystem, $type, $collectionSet);
        }

        $app['redis']->set('assetic|version', $thisVersion);
    }

    public static function CompileCollection($app, $collection)
    {
        $thisVersion = $app['redis']->get('assetic|version');
        if ( empty( $thisVersion ) ) {
            $thisVersion = dechex( time() );
            $app['redis']->set('assetic|version', $thisVersion);
        }

        $client = $app['aws']->get('s3');
        $S3Bucket = $app['config']['application']['mediaS3Bucket'];
        $filesystem = new Filesystem(new Adapter($client, $S3Bucket));

        list($type, $collectionSet) = explode("|", $collection);
        return self::GenerateAsset($app, $thisVersion, $filesystem, $type, $collectionSet);
    }

    public static function GenerateAsset($app, $thisVersion, $filesystem, $type, $collectionSet)
    {
        $filesList = explode(",", $collectionSet);
        $thisCollectionKey = md5($collectionSet);   

        $fileName = self::GenerateFilename($type, $thisCollectionKey, $thisVersion);

        $localAssetPath = $app['app_path'] . '/web' ;

        $localDistPath = $app['app_path'] . '/web/' . $type . 'dist' ;
        $s3DistPath = '/' . $type ;
        $localTargetFile = $localDistPath . "/" . $fileName ;
        $s3TargetFile = $s3DistPath . "/" . $fileName ;

        $gzfile = $localTargetFile . ".gz";
        $s3TargetFileGzipped = str_replace(".".$type, ".gz.".$type, $s3TargetFile);

        echo "Preparing asset for $collectionSet of $type, local file = $localTargetFile \n";
        if ( !file_exists($localTargetFile) ) {
            echo "No local file, generating \n";
            $fullPathList = [];
            foreach ( $filesList as $f ) {
               $fullPathList[] = $localAssetPath . $f;
            }
     
            try {
                $am = new AssetManager();
                $factory = new AssetFactory( $localAssetPath );
                $factory->setAssetManager($am);
                     
                $fm = self::GetFilterManager( $type );
                if ( !empty( $fm ) ) {
                    $factory->setFilterManager($fm);
                }
                 
                $thisBundle = $factory->createAsset($fullPathList, array('yui_' . $type));
                 
                echo "Checking if $localDistPath exists\n";
                if ( !is_dir( $localDistPath )  ) {
                    echo "creating $localDistPath\n";
                    mkdir($localDistPath);
                }
         
                $thisBundle->setTargetPath( $fileName );
                $am->set('thisBundle', $thisBundle);
         
                echo "setup writer now\n";
                $writer = new AssetWriter($localDistPath);
                echo "writing asset now\n";
                $writer->writeManagerAssets($am);

                if ( file_exists($localTargetFile) ) {
                    echo "$localTargetFile generated! Generate gzipped version!\n";
                    $fp = gzopen ($gzfile, 'w9');
                    gzwrite ($fp, file_get_contents($localTargetFile));
                    gzclose($fp);
                } else {
                    echo "local file generation error, still missing after generation\n";
                    return false;
                }
            } catch (\Exception $e) {
                echo "encounter error when generating for $collectionSet \n";
                echo "Error is " . $e->getMessage() . "\n";
                return false;
            }
        }

        $stream = fopen($localTargetFile, 'r+');
        $copySuccess = $filesystem->putStream($s3TargetFile, $stream, [
            'visibility' => 'public',
            'mimetype' => ( $type == "css" ) ? 'text/css' : 'application/x-javascript',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'Expires'    => gmdate("D, d M Y H:i:s T", strtotime("+3 years")),
            'Cache-Control' => 'public,max-age=93312000',
            'Metadata' => ['Cache-Control' => 'public,max-age=93312000']
        ]);

        $gzstream = fopen($gzfile, 'r+');
        $copySuccess = $filesystem->putStream($s3TargetFileGzipped, $gzstream, [
            'visibility' => 'public',
            'mimetype' => ( $type == "css" ) ? 'text/css' : 'application/x-javascript',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'Expires'    => gmdate("D, d M Y H:i:s T", strtotime("+3 years")),
            'ContentEncoding' => 'gzip',
            'Cache-Control' => 'public,max-age=93312000',
            'Metadata' => ['Cache-Control' => 'public,max-age=93312000','Content-Encoding' => 'gzip']
        ]);

        if ( $filesystem->has($s3TargetFile) && $filesystem->has($s3TargetFileGzipped)  ) {
            echo "$s3TargetFile and $s3TargetFileGzipped found in S3!\n";
            $app['redis']->set('assetic|'.$thisCollectionKey, $thisVersion);
            return true;
        } else {
            echo "$s3TargetFile and $s3TargetFileGzipped not found in S3!\n";
            return false;
        }

    }

    public static function GetFilterManager($type)
    {
        switch($type)
        {
            case 'css':
                $fm = new FilterManager();
                $fm->set('yui_css', new Yui\CssCompressorFilter('/usr/share/yui-compressor/yui-compressor.jar'));
                return $fm;
            case 'js':
                $fm = new FilterManager();
                $fm->set('yui_js', new Yui\JsCompressorFilter('/usr/share/yui-compressor/yui-compressor.jar'));
                return $fm;
            default:
                return null;
                break;
        }
    }

    public static function GenerateWebPath($app, $type, $filename)
    {
        $url = "/" . $type . "/" . $filename;
        if ( !empty($app['config']['assetic']['baseDomain']) ) {
            $url = $app['config']['assetic']['baseDomain'] . $url;
        }

        $acceptEncoding = $app['request']->headers->get('Accept-Encoding');
        if ( strpos($acceptEncoding, "gzip") >= 0){
            $url = str_replace (".".$type, ".gz.".$type, $url);
        }

        return $url;
    }

    public static function GenerateFilename($type, $collectionKey, $version)
    {
        return $collectionKey . "-" . $version . "." . $type;
    }

}
