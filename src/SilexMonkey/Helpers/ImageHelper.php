<?php

namespace SilexMonkey\Helpers;

use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\Color;

class ImageHelper
{

    private $imagine = null;
    private $imageHandler = null;
    private $imageFilePath = null;
    private $library = "GD";

    private $outputDirectory = "/tmp/phptemp/";

    private $error = "";

    public function __construct($imageFilePath=null, $library="GD")
    {
        switch ($library) {
            case "Imagick":
                $this->imagine = new \Imagine\Imagick\Imagine();
                $this->library = 'imagick';
                break;
            case "GD":
            default:
                $this->imagine = new \Imagine\Gd\Imagine();
                break;
        }
    
        if ( !empty($imageFilePath) && is_readable( $imageFilePath ) ) {
            $this->openImage($imageFilePath);
            $this->imageFilePath = $imageFilePath;
        }

        $this->setOutputDirectory( "./" );
    }

    public function openImage($imageFilePath)
    {
        if ( ! is_readable($imageFilePath) ) {
            $this->error = "Unable to read the file $imageFilePath!";
            return false;
        }

        $this->imageHandler = $this->imagine->open($imageFilePath);
        $this->imageFilePath = $imageFilePath;
    }

    public function setOutputDirectory($outputDirectory)
    {
        if ( is_dir($outputDirectory) ) {
            if ( ! is_writable($outputDirectory) ) {
                $this->error = "Output file path $outputDirectory provided is not writable";
            }
        } else { 
            mkdir( $outputDirectory, 0744, true );
        }

        $this->outputDirectory = $outputDirectory;
        return true;
    }

    public function generateThumbnail($size, $outputFilename, $method='crop', $compressLevel = 70)
    {
        list($width, $height) = explode("x", $size);

        $width = (int)$width;
        $height = (int)$height;

        $destination = $this->outputDirectory . $outputFilename;
        $sourceSize = $this->imageHandler->getSize();
        $sourceWidth = $sourceSize->getWidth();
        $sourceHeight = $sourceSize->getHeight();

        if ( $height == 0 ) {
            $height = (int)$sourceHeight * ($width/$sourceWidth) ;
        } else if ( $width == 0 ) {
            $width = (int)$sourceWidth * ($height/$sourceHeight) ;
        }

        $newImage = $this->imageHandler->copy();

        $keepOriginalSize = ( $height == 0 && $sourceWidth <= $width ) || ( $width == 0 && $sourceHeight <= $height ) ;

        if ( !$keepOriginalSize ) {
         
            if ( $method == 'crop' ) { 
                if ( ( $sourceWidth / $sourceHeight ) > ( $width / $height ) ) {
                    $newWidth = ($sourceWidth/$sourceHeight) * $height ;
                    //echo "\n($sourceWidth/$sourceHeight) = ($newWidth/$height) \n";
                    $newImage->resize(new \Imagine\Image\Box($newWidth, $height))
                                ->crop(new \Imagine\Image\Point(0,0), new \Imagine\Image\Box($width, $height))
                                ->interlace(\Imagine\Image\ImageInterface::INTERLACE_LINE)
                                ->save($destination, array('jpeg_quality' => $compressLevel));
                     
                } else if ( ( $sourceWidth / $sourceHeight ) < ( $width / $height ) ) {
                    $newHeight = $width / ($sourceWidth/$sourceHeight) ;
                    //echo "\n($sourceWidth/$sourceHeight) = ($width/$newHeight) \n";
                    $newImage->resize(new \Imagine\Image\Box($width, $newHeight))
                                ->crop(new \Imagine\Image\Point(0,0), new \Imagine\Image\Box($width, $height))
                                ->interlace(\Imagine\Image\ImageInterface::INTERLACE_LINE)
                                ->save($destination, array('jpeg_quality' => $compressLevel));
             
                } else {
                    $newImage->resize(new \Imagine\Image\Box($width, $height))->interlace(\Imagine\Image\ImageInterface::INTERLACE_LINE)->save($destination, array('jpeg_quality' => $compressLevel));
                }
            } else if ( $method == 'resize' ) {
                $newImage->thumbnail(new \Imagine\Image\Box($width, $height))->interlace(\Imagine\Image\ImageInterface::INTERLACE_LINE)->save($destination, array('jpeg_quality' => $compressLevel));
            }
        } else {
            $newImage->interlace(\Imagine\Image\ImageInterface::INTERLACE_LINE)
                    ->save($destination, array('jpeg_quality' => $compressLevel));
        }

        return is_file($destination) ;
    }

    public function optimizeImage($compressLevel = 70)
    {
        $destination = $this->imageFilePath . ".new.jpg";
        $this->imageHandler
                ->interlace(\Imagine\Image\ImageInterface::INTERLACE_LINE)
                ->save($destination, array('jpeg_quality' => $compressLevel));
        return is_file($destination) ;
    }

}
