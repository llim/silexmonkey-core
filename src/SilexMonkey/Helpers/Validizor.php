<?php

namespace SilexMonkey\Helpers;

class Validizor
{
    private static $changes = array();
    private static $logs = array();
    private static $numberOfChange = 0;

	public static function validize(&$targets, $rules)
	{
        self::$numberOfChange = 0;
        if ( empty($targets) ) {
            return null;
        }         

        if ( empty($rules) ) {
            return $targets;
        }         

        $changes = array();
        foreach ( $targets as $fieldName => $fieldValue ) {
            if ( empty($rules[$fieldName]) ) {
                continue;
            }
            $result = self::iterateRules($fieldName, $fieldValue, $rules);
            if ( $result !== $fieldValue ) {
                self::$numberOfChange += 1;
                self::$changes[$fieldName] = array($fieldValue => $result);
                $targets[$fieldName] = $result;
            }
            array_push(self::$logs, "validize $fieldName with " . print_r($rules[$fieldName], true) . " and result is *$result*.");
        }

        return $targets;
	}

    public static function getChanges()
    {
       return self::$changes;
    }

    public static function getLogs()
    {
       return self::$logs;
    }

    public static function getNumberOfChange()
    {
       return self::$numberOfChange;
    }

    public static function iterateRules($fieldName, &$fieldValue, $rules) {
        $change = 0;
        foreach ( $rules[$fieldName] as $rule ) {
            if ( empty($rule['type']) ) {
                $newValue = $fieldValue;
                continue;
            }

            if ( empty($rule['cast']) ) {
                $rule['cast'] = false;
            }

            if ( empty($rule['rule']) ) {
                // default rule parameter should be just empty
                $rule['rule'] = '';
            }

            $newValue = self::applyRule($fieldValue, $rule['type'], $rule['rule'], $rule['cast']);
        }
        
        return $newValue;
    }

    public static function applyRule($target, $ruleType, $rule, $typeCast) {
        switch( $ruleType ) {
            case 'maxlength':
                return substr($target, 0, $rule);
            case 'lowercase':
                return strtolower($target);
            case 'uppercase':
                return strtoupper($target);
            case 'integer':
                $target = filter_var($target, FILTER_SANITIZE_NUMBER_INT);
                // http://stackoverflow.com/questions/239136/fastest-way-to-convert-string-to-integer-in-php
                return ( $typeCast ) ? $target+0 : $target ;
            case 'float':
                $target = filter_var($target, FILTER_SANITIZE_NUMBER_FLOAT);
                // http://stackoverflow.com/questions/239136/fastest-way-to-convert-string-to-integer-in-php
                return ( $typeCast ) ? floatval($target) : $target ;
            case 'numeric':
                $target = filter_var(filter_var($target, FILTER_SANITIZE_NUMBER_FLOAT), FILTER_VALIDATE_FLOAT) ? $target : null;
                return ( $typeCast ) ? floatval($target) : $target ;
            case 'boolean':
                return filter_var($target, FILTER_VALIDATE_BOOLEAN) ? (bool) $target : null;
            case 'inarray':
                return (in_array($target, $rule)) ? $target : null;
            case 'regex':
                $match = array();
                return (preg_match($rule,$target,$match) > 0) ? $match[1] : null;
            case 'stripnewline':
                return str_replace(PHP_EOL, '', $target);
            case 'nomultibyte':
                return self::anyMultiByte($target) ? '' : $target;
            case 'email':
                return filter_var(filter_var($target, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL) ? $target : null;
            case 'ipv4':
                return filter_var($target, FILTER_VALIDATE_IP) ? $target : null ;
            case 'url':
                return filter_var(filter_var($target, FILTER_SANITIZE_URL), FILTER_VALIDATE_URL) ? $target : null;
            case 'urlencoded':
                return filter_var($target, FILTER_SANITIZE_ENCODED);
            case 'htmlescape':
                return filter_var($target, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            case 'mongoid':
                return \MongoId::isValid($target);
            case 'mongodate':
                return ( is_a($target, 'MongoDate') ? $target : ( $typeCast ? new \MongoDate(strtotime( $target ) ) : null ) );
            default:
                return $target;
        }
    }

    public static function anyMultibyte($string)
    {
        return !mb_check_encoding($string, 'ASCII') && mb_check_encoding($string, 'UTF-8');
    }
}
