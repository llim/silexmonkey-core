<?php

namespace SilexMonkey\Helpers;

class Bucket
{
    public static function inBucket($key, $numberOfBucket, $targetBucket)
    {
        $crc = crc32($key);
        return ($targetBucket == ($crc % $numberOfBucket));
    }

    public static function whichBucket($key, $numberOfBucket)
    {
        $crc = crc32($key);
        return $crc % $numberOfBucket;
    }
}
