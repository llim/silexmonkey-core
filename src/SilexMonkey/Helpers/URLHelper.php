<?php

namespace SilexMonkey\Helpers;

class URLHelper 
{
    public static function generateBaseUrl( $app, $subdomain = null )
    {
        return ( ($app['config']['control']['https']) ? 'https' : 'http' ) . 
            "://" .
            ( $subdomain ? $subdomain . "." : '' ) . 
            ( $app['env'] == 'prod' ? '' : ( $app['env'] . '.' ) ) .
            $app['config']['application']['basedomain'];
    }
}
