<?php

namespace SilexMonkey\Helpers\OAuth;

use OAuth\UserData\ExtractorFactory;

class OAuthProviderHelper
{

    private $oAuthProvider;
    protected $app;
    public static $error = "";

    public function __construct( &$app )
    {
        $this->app = $app;
    }

    public static function extractInfo($provider, $service, $serviceUserInfoEndPoint)
    {
        $userInfoFromProvider = json_decode($service->request($serviceUserInfoEndPoint), true);

        $extractorFactory = new ExtractorFactory();
        $infoExtractor = $extractorFactory->get($service);

        $tokenStorage = $service->getStorage();
        $token = $tokenStorage->retrieveAccessToken($provider)->getAccessToken();
        $lastName = ($infoExtractor->supportsLastName()) ? $infoExtractor->getLastName() : null ;
        $firstName = ($infoExtractor->supportsFirstName()) ? $infoExtractor->getFirstName() : null ;
        
        $fullName = ($infoExtractor->supportsFullName()) ? $infoExtractor->getFullName() : null ;

        if ( empty($lastName) && empty($firstName) && !empty($fullName) ) {
            $tempName = explode(" ", $fullName);
            $lastName = array_pop($tempName);
            $firstName = implode(" ", $tempName);
        }

        if ( $infoExtractor->supportsExtra()  ) {
            $extra = $infoExtractor->getExtras();
        } else {
            $extra = [];
        }

        $userInfo = array(
            'email' => ($infoExtractor->supportsEmail()) ? $infoExtractor->getEmail() : '' ,
            'lastName' => $lastName,
            'firstName' => $firstName,
            'location' => ($infoExtractor->supportsLocation()) ? $infoExtractor->getLocation() : '' ,
            'extra' => $extra,
            'socialId' => array(
                'provider' => $provider,
                'uniqueId' => ($infoExtractor->supportsUniqueId()) ? $infoExtractor->getUniqueId() . "" : $infoExtractor->getUsername() ,
                'profileImageUrl' => ($infoExtractor->supportsImageUrl()) ? $infoExtractor->getImageUrl() : '' ,
                'accessToken' => $token
            )
        );

        return $userInfo;
    }
    
}
