<?php

namespace SilexMonkey\Helpers\OAuth;

use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;

class OAuthProviderFactory
{

    private $oAuthProvider;
    protected $app;
    public static $error = "";

    public static function get(\Silex\Application $app, $serviceProvider)
    {
        $oAuthConfig = $app['oauth'];
        if ( !empty($oAuthConfig[$serviceProvider]) ) {
            $thisServiceKey = $oAuthConfig[$serviceProvider]['key'];
            $thisServiceSecret = $oAuthConfig[$serviceProvider]['secret'];
        } else {
            $this->error = "config for provider " . $serviceProvider . " not found";
            return null;
        }

        if ( !empty( $oAuthConfig['callback'] ) ) {
            $callbackUrl = $oAuthConfig['callback'] ;
            $callbackUrl = str_replace('{provider}', $serviceProvider, $callbackUrl);
        } else {
            $this->error = "no callback url defined";
            return null;
        }

        if ( !empty($oAuthConfig[$serviceProvider]['scope']) ) {
            $scope = $oAuthConfig[$serviceProvider]['scope'] ;
        } else {
            $scope = [];
        }

        $storage = new Session();
        $credentials = new Credentials(
            $thisServiceKey,
            $thisServiceSecret,
            $callbackUrl
            );

        $serviceFactory = new \OAuth\ServiceFactory();
        return $serviceFactory->createService($serviceProvider, $credentials, $storage, $scope);
    }

    public static function getAuthorizationUri($provider, $service)
    {
        switch($provider)
        {
            case 'facebook':
                return $service->getAuthorizationUri() . "";
            case 'linkedin':
                return $service->getAuthorizationUri() . "&state=" . \SilexMonkey\Helpers\Randomizer::generateRandomCode(16, 'alphanum');
            case 'twitter':
                $token = $service->requestRequestToken();
                return $service->getAuthorizationUri(array('oauth_token' => $token->getRequestToken())) ."";
            default:
                return null;
        }
    }

    public static function getOAuthToken($provider, $service, $getParameters)
    {
        switch($provider)
        {
            case 'facebook':
                $authorizationToken = $getParameters['code'];
                return $service->requestAccessToken($authorizationToken);
            case 'twitter':
                $authorizationToken = $getParameters['oauth_token'];
                $verifier = $getParameters['oauth_verifier'];
                $storage = new Session();
                $token = $storage->retrieveAccessToken('Twitter');
                return $service->requestAccessToken($authorizationToken, $verifier, $token->getRequestTokenSecret());
            case 'linkedin':
                $state = ( !empty($getParameters['state']) ? $getParameters['state'] : null );
                return $service->requestAccessToken($getParameters['code'], $state);
            default:
                return null;
        }
    }


    public static function getError()
    {
        return $this->error;
    }
}
