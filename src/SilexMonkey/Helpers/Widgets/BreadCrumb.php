<?php

namespace SilexMonkey\Helpers\Widgets;

class BreadCrumb 
{
    public static function generate($list, $activeItem)
    {
        $returnArray = array();
        $returnArray['breadcrumb'] = array();

        for ( $n = 0; $n<count($list); $n++ )
        {
            $returnArray['breadcrumb'][] = array(
                        'url' => $list[$n]['url'], 
                        'location' => $list[$n]['location'], 
                        'active' => ( $list[$n]['location'] == $activeItem ) ? true : false 
            );

        }
        return $returnArray;
    }
}
