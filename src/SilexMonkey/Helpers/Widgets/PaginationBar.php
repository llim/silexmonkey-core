<?php

namespace SilexMonkey\Helpers\Widgets;

class PaginationBar 
{
    public static function generate($itemsTotal, $perPageLimit, $baseUrl, $queryParameters = array(),  $currentPage = 1, $barSize = 10)
    {

        $currentPage = $currentPage == 0 ? 1 : $currentPage;
        $returnData = array();
        $returnData['paginate'] = true;
        $returnData['paginateDebug'] = array();
        $totalPages = ceil($itemsTotal/$perPageLimit);

        $start = ( $currentPage <= (floor($barSize/2)-1) ? 1 : ( $currentPage - floor($barSize/2) ) ); 
        $start = ( $currentPage == 5 ) ? 1 : $start;
        $middle = ( $currentPage < floor($barSize/2) ? floor($barSize/2) : $currentPage ); 
        $end = ( $currentPage < floor($barSize/2) ? 10 : floor($barSize/2) + $currentPage ); 
        $end = ( $end >= $totalPages ) ? $totalPages : $end ;

        if ( $totalPages > $end ) {
            $returnData['morePages'] = array("thisPage" => $end+1, "baseUrl" => $baseUrl, "queryParameter" => $queryParameters) ;
        }

        $returnData['pages'] = array();
        for($n=$start;$n<=$end;$n++)
        {
            $returnData['pages'][] = array("thisPage" => $n, "baseUrl" => $baseUrl, "queryParameter" => $queryParameters, "currentPage" => ($n == $currentPage) ) ;
        }

        if ( $currentPage > 1 ) {
            $returnData['prevPage'] = array("thisPage" => ( ($start == 1 ) ? 1 : $start-1 ) , "baseUrl" => $baseUrl, "queryParameter" => $queryParameters );
        }

        if ( $currentPage < $totalPages ) {
            $returnData['nextPage'] = array("thisPage" => ( ($end<$totalPages) ? $end+1 : $end ) , "baseUrl" => $baseUrl, "queryParameter" => $queryParameters );
        }

        $returnData['paginateDebug']['start'] = $start;
        $returnData['paginateDebug']['end'] = $end;
        $returnData['paginateDebug']['totalPages'] = $totalPages;
        $returnData['paginateDebug']['pages'] = $returnData['pages'];

        return $returnData;
    }
}
