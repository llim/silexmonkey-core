<?php

namespace SilexMonkey\Helpers\Twilio;

class SMS
{
    private static $error = "";

    public static function send($twilio, $from, $to, $message, $callback = null)
    {
        try {
            $status = $twilio->account->messages->create(array(
                "From" => $from,
                "To" => $to,
                "Body" => substr($message, 0 , 135),
            ));
        } catch (\Services_Twilio_RestException $e) {
            self::$error = $e->getMessage();
            echo "Error Message is :\n" . $e->getMessage() . "\n";
            return false;
        }

        return $status;
    }

    public static function getLastError()
    {
        return self::$error;
    }
}
