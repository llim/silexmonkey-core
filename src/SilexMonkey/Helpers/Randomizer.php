<?php

namespace SilexMonkey\Helpers;

class Randomizer
{
	public static function generateRandomCode($length=32, $type="alphanumsym")
	{
        $baseChar = self::getBaseString( $type ) ;
        return self::selectString($baseChar, $length);
	}

    private static function selectString ( $randBaseChar,  $size = 8 )
    {
        $randomString = '';
        $baseSize = strlen( $randBaseChar )-1;
        for ($i = 0; $i<$size ; $i++) {
            $randomOffset = rand(0,$baseSize);
            $randomString .= substr($randBaseChar, $randomOffset, 1);
        }
        return $randomString;
    }
    
    private static function getBaseString ( $type )
    {
        switch ($type) {
            case 'alpha':
                return 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            case 'alphanum':
                return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            case 'alpahnumsym':
            default:
                return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+{}[]|;:<>,.?/-=';
        }
    }

}
