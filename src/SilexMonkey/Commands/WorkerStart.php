<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class WorkerStart extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('worker')
            ->setDescription('start a worker to work on messages in queue')
            ->addOption('queue', '', InputOption::VALUE_OPTIONAL , 'Queue Name')
            ->setHelp('Usage: <info>./console.php worker [--queue=queuename]</info>')
        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $app = $this->getSilexApplication();
        if ($queueName = $input->getOption('queue'))
        {
            $app['queue']->setQueue( $queueName );
        }
             
        $output->write("\n\tStarted worker for Queue = " . $app['queue']->getQueue() . "\n");
        $worker = new \SilexMonkey\Queue\QueueWorker( $app );
        $worker->working();
    }
}
