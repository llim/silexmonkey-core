<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AddJob extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('worker:addjob')
            ->setDescription('start a worker to work on messages in queue')
            ->addOption('queue', '', InputOption::VALUE_OPTIONAL , 'Queue Name')
            ->addOption('payload', '', InputOption::VALUE_REQUIRED, 'Job Payload')
            ->addOption('class', '', InputOption::VALUE_REQUIRED, 'Job Class')
            ->addOption('following', '', InputOption::VALUE_OPTIONAL, 'Job Following')
        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $app = $this->getSilexApplication();
        $queueMaster = $app['queue'];
            
        if ($queueName = $input->getOption('queue')) {
            $queueMaster->setQueue( $queueName );
        }
            
        if ($following = $input->getOption('following')) {
            $following = json_decode($following, true);
        } else {
            $following = array();
        }
            
        $queueMaster->addJob(
            $input->getOption('class'),
            json_decode( $input->getOption('payload'), true ),
            $following
        );
            
        $output->write("\n\tJob added to queue (" . $queueMaster->getQueue() . ")\n");
    }
}
