<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RequeueJob extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('worker:requeuejob')
            ->setDescription('requeue job')
            ->addOption('queue', '', InputOption::VALUE_OPTIONAL , 'Queue Name')
            ->addOption('jobid', '', InputOption::VALUE_REQUIRED, 'Job ID')
        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $app = $this->getSilexApplication();
        $queueMaster = $app['queue'];

        if ($queueName = $input->getOption('queue')) {
            $queueMaster->setQueue( $queueName );
        }
            
        $queueMaster->requeueJob( $input->getOption('jobid') );
        $output->write("\n\tJob requeue to queue (" . $queueMaster->getQueue() . ")\n");
    }
}
