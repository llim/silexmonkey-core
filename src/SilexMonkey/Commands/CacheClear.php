<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CacheClear extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('cache:clear')
            ->setDescription('clear the cache')
            ->setHelp('Usage: <info>./console.php cache:clear</info>')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->write("\n\tClearing cache\n\n");
    }
}
