<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AsseticGeneration extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('assetic:generate')
            ->setDescription('Compile and generate combined/minified asset.')
        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $app = $this->getSilexApplication();
        $output->write("\n\tGenerating Asset");
        $output->write("\n");

        \SilexMonkey\Helpers\Assetic::Compile($app);
        $output->write("\n\tAsset Generation completed!");
        $output->write("\n");
    }
}
