<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Hello extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('hello')
            ->setDescription('Hello world.')
            ->addArgument(
                'userId',
                InputArgument::OPTIONAL,
                'Say hello to this user'
                )
            ->addOption(
                'debug',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will run in debug mode'
                )
        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $app = $this->getSilexApplication();
        $output->write("\n\tHello " . $input->getArgument('userId') . "!");
        $output->write("\n");

        if ( $input->getOption('debug') ) {
            $output->write("\n\tDebug mode is on!\n");
        }
    }
}
