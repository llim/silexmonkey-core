<?php

namespace SilexMonkey\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class QueueSize extends \Knp\Command\Command {

    protected function configure() {
        $this
            ->setName('worker:queuesize')
            ->setDescription('get the current queue size')
            ->addOption('queue', '', InputOption::VALUE_OPTIONAL , 'Queue Name')
        ; // nice, new line
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $app = $this->getSilexApplication();
        $queueMaster = $app['queue'];
            
        if (!$queueName = $input->getOption('queue')) {
            $queueName = 'default';
        }
                 
        $size = $queueMaster->getQueueSize( $queueName );
        $output->write("$size");
    }
}
