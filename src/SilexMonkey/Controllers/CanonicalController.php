<?php

//This is a standard controller which will remove the trailing slash and redirect to the new url
//To use this controller, put it at the bottom of the routing configuration file:
//remove_trailing_slash:
//    path: /{url}
//    defaults:
//        _controller: SilexMonkey\Controllers\CanonicalController::removeTrailingSlash
//    requirements:
//        url: .*/$
//    methods: [GET]


namespace SilexMonkey\Controllers;

use Symfony\Component\HttpFoundation\Request;

class CanonicalController extends BaseController
{
    public function removeTrailingSlash(Request $request)
    {
        $pathInfo = $request->getPathInfo();
        $requestUri = $request->getRequestUri();

        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

        return $this->app->redirect($url, 301);
    }
}
