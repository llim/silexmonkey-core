<?php

namespace SilexMonkey\Controllers;

use Silex\Application;

/**
 * Base class for application controllers.
 *
 * @author Monkey <leong_kui@yahoo.com>
 */
abstract class BaseController
{
    protected $app;
    protected $outputData;
    protected $inputData;
    protected $formData;
    protected $appData;
    protected $trackingData;
    protected $contextData;
    protected $debugData;

    protected $loginId = null;
    protected $userProfile = null;

    protected $userType = null;
    protected $requiredUserType = null;
    protected $userStatus = null;
    protected $requiredUserStatus = null;
    protected $userRole = null;
    protected $requiredUserRole = null;

    /**
     * Constructor.
     *
     * @param Application $app
     */
    public function __construct(Application &$app=null)
    {
        if ( !empty($app) ) {
            $this->app = $app;
        }
    }

    protected function extraConstruct()
    {
        $this->outputData['css'] = array();
        $this->outputData['jsInHeader'] = array();
        $this->outputData['jsInFooter'] = array();

        // feature toggle
        // in app.yml, specify a section of title 'feature'
        // e.g.:
        //    someCoolFeatureEnabled: true
        //  in the code
        //    if ( $this->outputData['someCoolFeatureEnabled'] ) { print "awesome new cool feature!!" ; }
        //  or in template
        //    {{#someCoolFeatureEnabled}} COOOOOOL {{/someCoolFeatureEnabled}}
        if ( !empty($this->app['config']['feature']) ) {
            foreach ( $this->app['config']['feature'] as $feature => $toggle )
            {
                $this->outputData[$feature] = $toggle;
            }
        }
    }

    public function injectApp(Application &$app)
    {
        if ( !empty($app) ) {
            $this->app = $app;
            $this->outputData = $this->app['outputData'];
            $this->inputData = $this->app['inputData'];
            $this->appData = $this->app['appData'];
            $this->formData = $this->app['formData'];
            $this->trackingData = $this->app['trackingData'];
            $this->contextData = $this->app['contextData'];
            $this->debugData = array();

            if ( !isset($this->trackingData['page']) ) {
                $this->trackingData['page'] = [];
            }

            $this->userSetup();

            $this->extraConstruct();
        }
    }

    public function injectCss($css)
    {
        $this->outputData['css'][] = $css;
    }

    protected function userSetup()
    {
        $this->outputData['sessionId'] = $this->app['contextData']['session']['id'];
        if ( !empty($this->appData['user']) ) {
            if ( !empty($this->appData['user']['loginId'] ) ) {
                $this->loginId = $this->appData['user']['loginId'];
            }
            if ( !empty($this->appData['user']['type'] ) ) {
                $this->userType = $this->appData['user']['type'];
            }
            if ( !empty($this->appData['user']['profile'] ) ) {
                $this->userProfile = $this->appData['user']['profile'];
            }
            if ( !empty($this->appData['user']['profile']['status'] ) ) {
                $this->userStatus = $this->appData['user']['profile']['status'];
            }
            if ( !empty($this->appData['user']['status'] ) ) {
                $this->userStatus = $this->appData['user']['status'];
            }
        }

        if ( !empty($this->loginId) ) {
            $this->outputData['logoutCsrf'] = $this->app['form.csrf_provider']->generateCsrfToken('logout');
        } else {
            $this->outputData['loginCsrf'] = $this->app['form.csrf_provider']->generateCsrfToken('login');
        }

        if ( !empty($this->loginId) && !empty($this->requiredUserType) ) {
            if (!( !empty($this->userType) && in_array($this->userType, $this->requiredUserType))) {
                $this->loginId = null;
                $this->app['monolog']->error('User Account Type mismatch, required (' . implode(",",$this->requiredUserType) . '), user is ' . $this->userType );
            } else {
                $this->outputData['loginId'] = $this->loginId ;
            }
        }

        if ( !empty($this->loginId) && !empty($this->requiredUserStatus) ) {
            if (!( !empty($this->userStatus) && in_array($this->userStatus, $this->requiredUserStatus))) {
                $this->loginId = null;
                $this->app['monolog']->error('User Account Status mismatch, required (' . implode(",",$this->requiredUserStatus) . '), user is ' . $this->userStatus );
            }
        }

        if ( !empty($this->loginId) && !empty($this->requiredUserRole) ) {
            if (!( !empty($this->userRole) && in_array($this->userRole, $this->requiredUserRole))) {
                $this->loginId = null;
                $this->app['monolog']->error('User Account Role mismatch, required (' . implode(",",$this->requiredUserRole) . '), user is ' . $this->userRole );
            }
        }

        if ( !empty($this->loginId) ) {
                $this->outputData['username'] = empty($this->userProfile['firstName']) ? $this->loginId : $this->userProfile['firstName'];
                $this->outputData['loginId'] = $this->loginId;
        }
    }

    public function injectJavascript($js, $header=false)
    {
        if ( $header ) {
            $this->outputData['jsInHeader'][] = $js;
        } else {
            $this->outputData['jsInFooter'][] = $js;
        }
    }

    protected function constructHeader()
    {
    }

    protected function constructFooter()
    {
        $this->outputData['companyName'] = $this->app['config']['application']['companyName'];
        $this->outputData['year'] = date('Y');

        if ( !empty($this->formData['_debug']) ) {
            $this->outputData['debug'] = true;
            $this->outputData['debugData'] = $this->debugData;
            $this->outputData['debugData'][] = array("field"=>"outputData", "value" => print_r($this->outputData, true));
            $this->outputData['debugData'][] = array("field"=>"loginId", "value" => $this->loginId);
            $this->outputData['debugData'][] = array("field"=>"userType", "value" => $this->userType);
            $this->outputData['debugData'][] = array("field"=>"userStatus", "value" => $this->userStatus);
        }
    }

    public function render($template)
    {
        if ( !empty($this->app['config']['application']['css']) ) {
            $predefinedCss = $this->app['config']['application']['css'];
            if ( !empty($this->outputData['css']) ) {
                $this->outputData['css'] = array_merge($predefinedCss, $this->outputData['css']);
            } else {
                $this->outputData['css'] = $predefinedCss;
            }
        }

        if ( !empty($this->app['config']['application']['jsInHeader']) ) {
            $predefinedJsInHeader = $this->app['config']['application']['jsInHeader'];
            if ( !empty($this->outputData['jsInHeader']) ) {
                $this->outputData['jsInHeader'] = array_merge($predefinedJsInHeader, $this->outputData['jsInHeader']);
            } else {
                $this->outputData['jsInHeader'] = $predefinedJsInHeader;
            }
        }

        if ( !empty($this->app['config']['application']['jsInFooter']) ) {
            $predefinedJsInFooter = $this->app['config']['application']['jsInFooter'];
            if ( !empty($this->outputData['jsInFooter']) ) {
                $this->outputData['jsInFooter'] = array_merge($predefinedJsInFooter, $this->outputData['jsInFooter']);
            } else {
                $this->outputData['jsInFooter'] = $predefinedJsInFooter;
            }
        }

        if ( !empty($this->app['config']['control']['assetic']) && $this->app['config']['control']['assetic'] ) {
            $this->outputData['css'] = \SilexMonkey\Helpers\Assetic::GetCompiledCss( $this->app, $this->outputData['css'] );
            $this->outputData['jsInHeader'] = \SilexMonkey\Helpers\Assetic::GetCompiledJs( $this->app, $this->outputData['jsInHeader'] );
            $this->outputData['jsInFooter'] = \SilexMonkey\Helpers\Assetic::GetCompiledJs( $this->app, $this->outputData['jsInFooter'] );
        }

        if ( !empty($this->app['config']['application']['siteDescription']) && empty( $this->outputData['siteDescription'] ) ) {
            $this->outputData['siteDescription'] = $this->app['config']['application']['siteDescription'];
        }

        if ( !empty( $this->appData['error'] ) ) {
            $this->outputData['error'] = $this->appData['error'] ;
        }

        $this->app['outputData'] = array_merge($this->app['outputData'], $this->outputData);
        $languages = $this->getLanguages();
        if ( !empty($languages) ) {
            $this->outputData['languageSelection'] = $languages;
        }

        $this->outputData['app'] = [];
        $this->outputData['app']['lang'] = $this->app['language'];

        $this->constructHeader();
        $this->constructFooter();

        $this->app['appData'] = array_merge($this->app['appData'], $this->appData);
        $this->app['trackingData'] = array_merge($this->app['trackingData'], $this->trackingData);
        return $this->app['mustache']->render($template, $this->outputData);
    }

    protected function validateCsrfToken($intention)
    {
        return $this->app['form.csrf_provider']->isCsrfTokenValid($intention, $this->formData['_csrf_token']);
    }

    public function json($returnCode=200)
    {
        $this->appData['jsonOut'] = true;
        $this->app['appData'] = array_merge($this->app['appData'], $this->appData);
        $this->app['trackingData'] = array_merge($this->app['trackingData'], $this->trackingData);
        return $this->app->json($this->outputData, $returnCode);
    }

    protected function addDebug($message)
    {
        $debugTrace = '';
        if ( !empty($this->app['config']['control']['logCallerInDebug'])
         && $this->app['config']['control']['logCallerInDebug'] ) {
            $trace = debug_backtrace(false);

            $debugTrace =
                ( !empty($this->app['txid']) ? '['.$this->app['txid'].']' : '' ) .
                ( !empty($trace[1]['class']) ? ($trace[1]['class'] . (!empty($trace[1]['type']) ? $trace[1]['type'] : '::')) : '' ) .
                ( !empty($trace[1]['function']) ? $trace[1]['function'] : '' ) .
                ( !empty($trace[1]['line']) ? '#'. $trace[1]['line'] : '' )  .
                '>';
        }

        $this->app['monolog']->addDebug( $debugTrace . $message );
    }

    protected function getLanguages()
    {
        $cacheLocales = apc_fetch('silexmonkey.core.translation.locales');
        $locales = (!empty($cacheLocales)) ? $cacheLocales : [];

        $languages = [];
        $defaultLanguage = $this->app['language'];

        if ( $defaultLanguage != 'en' && in_array($defaultLanguage, $locales) ) {
            $locale = $defaultLanguage ;
            $thisLanguage = apc_fetch('silexmonkey.core.translation.set.'.$locale);
            $languages[] = ['locale' => $locale, 'language' => $thisLanguage ];
            if(($key = array_search($locale, $locales)) !== false) {
                unset($locales[$key]);
            }
        }

        $languages[] = ['locale' => 'en', 'language' => 'English'];
        $this->addDebug("supported locale en => English");
        foreach ( $locales as $locale )
        {
            $thisLanguage = apc_fetch('silexmonkey.core.translation.set.'.$locale);

            // skip english, as it will be the default
            if ( strlen($locale) > 0 && ( $thisLanguage != 'English' ) ) {
                $languages[] = ['locale' => $locale, 'language' => $thisLanguage];
                $this->addDebug("supported locale $locale => $thisLanguage");
            }
        }

        return $languages;
    }
}
