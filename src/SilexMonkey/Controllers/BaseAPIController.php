<?php

namespace SilexMonkey\Controllers;

use Silex\Application;
use SilexMonkey\Helpers\OAuthHelper;

/**
 * Base class for API controllers.
 *
 * @author Monkey <leong_kui@yahoo.com>
 */
abstract class BaseAPIController extends BaseController
{
    protected $requiredToken = true;
    protected $anonymousAccess = false;
    protected $user = array();
    protected $limit = 100;
    // page index start from 0
    protected $page = 0;

    protected function extraConstruct() {}

    protected function userSetup()
    {
        /* API use access validation */
    }

    public function render($template=null)
    {
        if ( $this->appData['statusCode'] ) {
            $returnCode = $this->appData['statusCode'];
        } else {
            $returnCode = ( empty($this->appData['error']) ) ? 200 : 500 ;
        }

        if ( !empty( $this->appData['error'] ) ) {
            $this->outputData['error'] = $this->appData['error'] ; 
        }

        $this->app['outputData'] = array_merge($this->app['outputData'], $this->outputData);
        return $this->json($returnCode); 
    }

    public function handle($version)
    {
        return $this->dispatcher($version,strtolower($this->app['request']->getMethod()));
    }

    public function get($version, $id=null)
    {
        return $this->dispatcher($version,'get', $id);
    }

    public function post($version)
    {
        return $this->dispatcher($version,'post');
    }

    public function put($version, $id=null)
    {
        return $this->dispatcher($version,'put', $id);
    }

    public function delete($version, $id=null)
    {
        return $this->dispatcher($version,'delete', $id);
    }

    protected function dispatcher($version, $verb, $id=null)
    {
        if ( !$this->accessValidation() ) {
            $this->appData['statusCode'] = 403;
            $this->appData['error'] = 'Access denied!';           
            return $this->render();
        }

        $thisVerb = $version . "Real" . ucfirst($verb);
        if ($this->app['request']->getMethod() == 'POST') {
            $this->addDebug("POST, no id required");
            return $this->{$thisVerb}();
        } else {
            $this->addDebug("id = *$id*");
            return $this->{$thisVerb}($id);
        }
    }

    protected function accessValidation()
    {
        if ( !empty($this->formData['_access_token']) ) {
            $this->app['accessToken'] = new \SilexMonkey\Models\Token($this->app, array("token" => $this->formData['_access_token']));
            $this->app['accessToken']->loadSelfFromDB();
            $this->addDebug("checking token data");
            $tokenUser = ($this->app['accessToken']->doesExist() ) ? $this->app['accessToken']->getLoginId() : null ;
        } else {
            $this->addDebug("No token");
            $this->app['accessToken'] = null;
        }

        // check session OR token for user first
        if ( !empty($this->contextData['session']['loginId']) ) { 
            $sessionUser =  $this->contextData['session']['loginId'];
        } else {
            $sessionUser = null;
        }

        if ( !empty($sessionUser) ) {
            $this->loginId = $sessionUser ;
            $this->addDebug("Using Session user");
        } else if ( !empty($tokenUser) ) {
            $this->loginId = $tokenUser ;
            $this->addDebug("Using token user");
        } else {
            $this->addDebug("No user information");
        }

        if ( empty($this->loginId) && !$this->anonymousAccess) {
            $this->appData['statusCode'] = 403;
            $this->appData['error'] = 'Access Denied.  (E9001)';
            return false;
        } 

        if ( $this->requiredToken && !$this->validateToken() && $this->app['accessToken'] ) {
            $this->appData['statusCode'] = 403;
            $this->appData['error'] = "Invalid token. (E9002)";
            return false;    
        }

        return true;
    }

    protected function validateToken()
    {
        if ( !empty($this->formData['_access_token']) ) {
            $this->addDebug("API: found token");
            $token = $this->formData['_access_token'] ;              
        } else {
            $this->addDebug("Token Missing");
            return false;
        }
        
        return OAuthHelper::checkAccess($this->app['accessToken']);
    }
}
