<?php

namespace SilexMonkey\Controllers;

class CommonObjectController extends \SilexMonkey\Controllers\BaseController
{

    protected $anonymousAccess = false;
    protected $objectName = "Dummy";
    protected $objectClass = "\SilexMonkey\Models\Dummy";
    protected $objectInstance = null;
    protected $objectIdField = 'id';

    protected $user = null;
    protected $csrfTerm = 'object';

    protected $offset = 0;
    protected $limit = 20;
    protected $paginationBarSize = 10;

    protected $id = null;

    protected $redirectUrl = '/';
    protected $listUrl = "/dummy/list";
    protected $editUrl = "/dummy/edit";
    protected $addUrl = "/dummy/add";
    protected $removeUrl = "/dummy/delete";

    protected $templates = array(
        'edit' => 'editobject',
        'list' => 'listobject',
        'view' => 'viewobject'
    );

    protected $css = array();
    protected $jsInHeader = array();
    protected $jsInFooter = array();

    protected $audit = null;
    protected $auditAction = array('create','update','delete', 'activate', 'deactivate');

    public function viewList($page=1)
   {
        if ( !$this->setup() ){
            return $this->app->redirect($this->redirectUrl);
        }

        $page = ($page==1) ? $this->offset : $page;
        $this->page = $page == 0 ? 1 : $page ;
        $this->skip = ( $page-1 >= 0 ? $page-1 : 0 );
        $this->outputData[lcfirst($this->objectName).'List'] = $this->getList();

        $count = count($this->outputData[lcfirst($this->objectName).'List']);
        for($n=0;$n<$count;$n++) {
            $this->outputData[lcfirst($this->objectName).'List'][$n]['counter'] = $n+1;
            $this->outputData[lcfirst($this->objectName).'List'][$n] = $this->transformItem( $this->outputData[lcfirst($this->objectName).'List'][$n], $n );
        }

        $this->addExtraOutput('post-list');
        $this->outputData['action'] = 'Edit';

        $totalHits = $this->objectInstance->count($this->getListCondition());
        $this->outputData['totalHits'] = $totalHits;
        if ( $totalHits > 0 ) {
            $paginationBar = \SilexMonkey\Helpers\Widgets\PaginationBar::generate(
                            $totalHits,
                            $this->limit,
                            $this->listUrl,
                            array(),
                            $this->page,
                            $this->paginationBarSize);
            $this->outputData = array_merge($this->outputData, $paginationBar);
        }

      return $this->render($this->templates['list']);
   }

    protected function transformItem($item, $itemOrder)
    {
        return $item;
    }

   public function view($id)
   {
        if ( !$this->setup($id) ){
            return $this->app->redirect($this->redirectUrl);
        }

        $this->addExtraOutput('view');

      return $this->render($this->templates['view']);
   }

   public function add()
   {
        if ( !$this->setup() ){
            return $this->app->redirect($this->redirectUrl);
        }

        $this->outputData['action'] = 'Create';
        $this->addExtraOutput('add');

      return $this->render($this->templates['edit']);
   }

   public function addPosted()
   {
        if ( !$this->setup() ){
            return $this->app->redirect($this->redirectUrl);
        }

        if ( !$this->validateInput() ){
            return $this->render($this->templates['edit']);
        }

        $this->addDebug("running add-pre step");
        $this->addExtraOutput('add-pre');
        $this->addDebug("running add default value step");
        $this->addDefaultValue();

        $this->addDebug("running require value check step");
        if ( !$this->objectInstance->allRequiredExists() ) {
            $this->appData['error'] = $this->objectInstance->getError() . ", E0003";
            return $this->render($this->templates['edit']);
        }

        $this->addDebug("running create step");
        $id = $this->objectInstance->create($this->inputData);
        if ( !empty($id) ) {
            $this->addDebug("running add post step");
            $this->addExtraOutput('add-post');
            $this->outputData['id'] = $id;
            $this->addDebug("running post create step");
            $this->postCreateAction();
            $this->recordAudit('Create');

            return $this->app->redirect($this->listUrl);
        } else {
            $this->addExtraOutput('add-post-fail');
            $this->addDebug("running add post faile step");
            $this->appData['error'] = 'Failed to create Object, E0004';
            $this->addDebug("failed to create object, error: ". $this->objectInstance->getError());
            return $this->render($this->templates['edit']);
        }
   }

   public function edit()
   {
        if ( !$this->setup() ){
            return $this->app->redirect($this->redirectUrl);
        }

        $this->outputData['action'] = 'Update';
        $this->addExtraOutput('edit');

      return $this->render($this->templates['edit']);
   }

   public function editPosted()
   {
        if ( !$this->setup() ){
            return $this->app->redirect($this->redirectUrl);
        }

        $this->outputData['action'] = 'Update';
        $this->addExtraOutput('edit-pre');
        $this->processInput();

        if ( !$this->validateInput() ){
          return $this->render($this->templates['edit']);
        }

        $objectInstance2 = new $this->objectClass($this->app, [$this->objectIdField => $this->inputData[$this->objectIdField]]);
        $objectInstance2->loadSelfFromDB();
        $dataInDB = $objectInstance2->toArray();

        // TODO!!!
        // I think the data removed from dataInDB is not considered
        $this->addDebug("Input Data = " . print_r($this->inputData, true) );
        $this->addDebug("Data in DB = " . print_r($dataInDB, true) );
        $diff = $this->diffUpdate($dataInDB);

        if ( count($diff) < 1 ) {
            $this->outputData = array_merge($this->outputData, $this->inputData);
            $this->appData['error'] = 'No change to object, E0005';
            return $this->render($this->templates['edit']);
        }

        $this->addDebug("Differences = " . print_r($diff, true) );

        $this->objectInstance->update($diff);
        $this->objectInstance->loadSelfFromDB();

        $this->postUpdateAction();
        $this->recordAudit('Update');

        $this->outputData = array_merge($this->outputData, $this->objectInstance->toArray());
        $this->addExtraOutput('edit-post');
        return $this->render($this->templates['edit']);
   }

   public function deletePosted()
   {
        return $this->executeStep('delete');
   }

   public function activateObject()
   {
        return $this->executeStep('activate');
   }

   public function deactivateObject()
   {
        return $this->executeStep('deactivate');
   }

   protected function executeStep($actionType)
   {
        if ( !$this->setup() ){
            return $this->app->redirect($this->redirectUrl);
        }

        if ( !$this->validateInput() ){
          return $this->render($this->templates['edit']);
        }

        $this->outputData['action'] = 'Update';
        $this->addExtraOutput('edit');

        switch( $actionType ) {
            case 'delete':
                $this->objectInstance->delete() ;
                $this->postDeleteAction();
                $this->recordAudit('Delete');
                break;
            case 'activate':
                $this->objectInstance->activate() ;
                $this->recordAudit('Activate');
                break;
            case 'deactivate':
                $this->objectInstance->deactivate() ;
                $this->recordAudit('Deactivate');
                break;
            default:
                break;
        }
        return $this->app->redirect($this->listUrl);
    }

    protected function addExtraOutput($section)
    {
    }

    protected function setup($id=null)
    {
        if ( (null === $this->loginId ) && !$this->anonymousAccess) {
            $this->appData['error'] = 'Please login before proceeding';
            return false;
        }

        if ( !empty($id) ) {
            $id = urldecode($id);
        }

        if ( !empty($this->inputData[$this->objectIdField]) ) {
            $this->id = $this->inputData[$this->objectIdField] ;
        } else if ( !empty($id) && empty($this->inputData[$this->objectIdField]) ) {
            $this->inputData[$this->objectIdField] = $id;
            $this->id = $id;
        }

        if ( !empty($this->inputData[$this->objectIdField]) ) {
            if ( strtolower($this->contextData['httpVerb']) != 'post' ) {
                $this->addDebug("Loading object data using " .  $this->inputData[$this->objectIdField] . " of field " . $this->objectIdField );
                $this->objectInstance = new $this->objectClass($this->app, array($this->objectIdField => $this->inputData[$this->objectIdField]));
                $this->objectInstance->loadSelfFromDB( [$this->objectIdField => $this->inputData[$this->objectIdField] ] );
                if ( strtolower($this->contextData['httpVerb']) == 'get' ) {
                    $this->addDebug("GET action , load data into outputData" );
                    $this->outputData = array_merge($this->objectInstance->toArray(), $this->outputData);
                }
            } else {
                $this->addDebug("POST action , only setup data object data using " .  $this->inputData[$this->objectIdField] . " of field " . $this->objectIdField );
                $this->objectInstance = new $this->objectClass($this->app, [$this->objectIdField => $this->inputData[$this->objectIdField]]);
            }
        } else if ( !empty($this->inputData) ) {
            $this->addDebug("setup object with only input data");
            $this->objectInstance = new $this->objectClass($this->app, $this->inputData);
        } else {
            $this->addDebug("setup object only");
            $this->objectInstance = new $this->objectClass($this->app);
        }

        foreach ( $this->css as $css ) {
            $this->injectCss( $css );
        }

        foreach ( $this->jsInHeader as $js ) {
            $this->injectJavascript( $js , true);
        }

        foreach ( $this->jsInFooter as $js ) {
            $this->injectJavascript( $js );
        }

        $this->outputData['csrfToken'] = $this->app['form.csrf_provider']->generateCsrfToken($this->csrfTerm);

        return true;
    }

    protected function validateInput()
    {
        if ( empty($this->inputData) ) {
            $this->appData['error'] = "Form error detected (" . $this->error . "), E0001";
            return false;
        }

        if ( !$this->validateCsrfToken($this->csrfTerm) ) {
            $this->appData['error'] = "Form Session expired, E001";
            return false;
        }

        return true;
    }

    protected function addDefaultValue()
    {
    }

    protected function diffUpdate($dataInDB)
    {
        return array_diff_assoc($this->inputData, $dataInDB);
    }

    protected function processInput()
    {
    }

    protected function postCreateAction()
    {
    }

    protected function postUpdateAction()
    {
    }

    protected function postDeleteAction()
    {
    }

    protected function recordAudit($action, $others = array())
    {
        if ( in_array(strtolower($action), $this->auditAction) ) {
            if ( !$this->audit ) {
                $this->audit = new \SilexMonkey\Models\Audit($this->app);
            }

            $data = array(
                'section' => strtolower($this->objectName),
                'action' => strtolower($action)
            );

            $data['data'] = $this->inputData;

            $data = array_merge($data, $others);
            $this->audit->add( $data );
        }
    }

    protected function getList()
    {
        return $this->objectInstance->retrieve($this->getListCondition(),$this->skip,$this->limit, $this->getSorting());
    }

    protected function getListCondition()
    {
        return array();
    }

    protected function getSorting()
    {
        return array();
    }
}
