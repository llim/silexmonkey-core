<?php

namespace SilexMonkey\Controllers;

use Silex\Application;

/**
 * Base class for API controllers.
 *
 * @author Monkey <leong_kui@yahoo.com>
 */
abstract class BaseMediaController extends BaseController
{
    protected $expiration = "+3 years";
    protected $maxAge = 93312000;
    protected $thumbnailSizes = ['800x800','300x300'];

    public function handleUpload($fieldName, $allowedExtension = array('png','jpg','jpeg'), $allowedMime = array('image/png', 'image/jpeg'))
    {
        $mediaLocalPath = $this->app['config']['application']['mediaLocalPath'];
        $maxUploadFileMax = $this->app['config']['application']['maxUploadFileMax'];

        if ( !file_exists($mediaLocalPath) || !is_writable($mediaLocalPath) ) {
            mkdir($mediaLocalPath);
        }

        $localStorage = new \Upload\Storage\FileSystem($mediaLocalPath);
        $file = new \Upload\File($fieldName, $localStorage);

        // rename the file on upload
        $newFilename = uniqid();
        $file->setName($newFilename);

        // Validate file upload
        // MimeType List => http://www.webmaster-toolkit.com/mime-types.shtml
        $file->addValidations(array(
            new \Upload\Validation\Mimetype($allowedMime),
            new \Upload\Validation\Extension($allowedExtension),
            new \Upload\Validation\Size($maxUploadFileMax)
        ));

        // Access data about the file that has been uploaded
        $this->inputData = array_merge($this->inputData, array(
            'name'       => $file->getNameWithExtension(),
            'extension'  => $file->getExtension(),
            'mime'       => $file->getMimetype(),
            'size'       => $file->getSize(),
            'md5'        => $file->getMd5(),
            'dimensions' => $file->getDimensions()
        ));

        $this->inputData['originalName'] = $_FILES[$fieldName]['name'];
        $this->inputData['status'] = 'ACTIVE';
        $this->inputData['storage'] = 'local';
        $this->inputData['localPath'] = $this->app['config']['application']['mediaLocalPath'] . "/" . $this->inputData['name'] ;

        // Try to upload file
        try {
            // Success!
            $file->upload();
            return true;
        } catch (\Exception $e) {
            // Fail!
            $error = $file->getErrors();
            $this->appData['statusCode'] = 500;
            $this->appData['error'] = (is_array($error)) ? implode(",", $error) : $error ;
            return false;
        }
    }

	public function commonUploadPosted($parameterArray = [])
	{
        //if (null === $this->loginId) {
		    //return $this->json(403);
        //}

        $this->outputData = [];
        $this->preProcessing($parameterArray);
        $this->inputData =  $this->inputDataSetup($parameterArray);

        if ( $this->handleUpload('file') ) {
            $this->addDebug('Media Upload ok! creating media record in DB');

            $thisMedia = new \SilexMonkey\Models\MediaModel($this->app, $this->inputData);
            if ( !$thisMedia->allRequiredExists() ) {
                $this->addDebug('Missing Media meta information: ' .$thisMedia->getError());
                $this->data = array('error' => $thisMedia->getError());
		        return $this->json(500);
            }

            $thisMediaId = $thisMedia->create();
            if ( $thisMediaId ) {
                $this->postMediaSetup($thisMediaId);

                $this->addDebug('Record created, copying to S3...');
                $thisMediaLocalPath = $this->inputData['localPath'];

                list($baseFilename, $extension, $leadingBucket, $targetS3Path) = $this->generatePathInfo($thisMediaLocalPath, $thisMediaId);
                $S3Bucket = $this->app['config']['application']['mediaS3Bucket'];
                $this->addDebug("Pushing $thisMediaLocalPath ($baseFilename) to s3://$S3Bucket/$targetS3Path to standard S3 storage");

                $thisSuccess = \SilexMonkey\Helpers\S3::copyToS3($this->app, $thisMediaLocalPath, $targetS3Path);

                if ( $thisSuccess ) {
                    $this->addDebug("file is on S3, removing local copy");
                    unlink($thisMediaLocalPath);

                    $this->addDebug("executing post success action");
                    $this->postS3Success($thisMediaId);

                    $thisMedia->loadSelfFromDB(['_id'=> new \MongoId($thisMediaId)]);
                    $thisMedia->update(['storage'=> 'S3']);

                    $queueMaster = $this->app['queue'];
                    $queueMaster->setQueue( $this->app['config']['application']['mediaProcessingQueue'] );
                    $queueMaster->addJob(
                        "ThumbnailGeneration",
                        array(
                            "mediaId" => $thisMediaId,
                            "sizes" => $this->thumbnailSizes,
                            "source" => $targetS3Path,
                            "sourceStorage" => "s3"
                        ),
                        array(
                            array("class" => "S3\\MediaMove", "payload" => array()),
                        )
                    );

                    $this->outputData = array("mediaId" => $thisMediaId);
		            return $this->json(200);
                } else {
                    $this->appData['error'] = 'Failed to move the media to S3';
                    return $this->json(500);
                }
            } else {
                $this->appData['error'] = "Failed to save media information!";
		        return $this->json(500);
            }
        } else {
            $this->addError('Media Upload failed due to ' . $this->appData['error']);
            $this->outputData = array('error' => $this->appData['error']);
		    return $this->json(500);
        }
	}

    protected function preProcessing($parameterArray)
    {
    }

    protected function inputDataSetup($parameterArray)
    {
        return $parameterArray;
    }

    protected function postMediaSetup($mediaId)
    {
    }

    protected function postS3Success($mediaId)
    {
    }

    protected function generatePathInfo( $thisMediaLocalPath, $thisMediaId)
    {
        $baseFilename = basename($thisMediaLocalPath);
        $leadingBucket = substr($thisMediaId, -1);
        $path = pathinfo($thisMediaLocalPath);
        $targetS3Path = $leadingBucket . "/" . $thisMediaId . "/" . $thisMediaId . '.' . $path['extension'];
        return [$baseFilename, $path['extension'], $leadingBucket, $targetS3Path];
    }

    protected function getMimeType( $extension )
    {
        switch(strtolower($extension))
        {
            case "png":
                return 'image/png';
            case "gif":
                return 'image/gif';
            case 'jpg':
            case 'jpeg':
                return 'image/jpeg';
            default:
                return 'image/jpeg';
        }
    }
}
