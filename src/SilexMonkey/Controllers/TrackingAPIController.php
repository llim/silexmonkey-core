<?php

namespace SilexMonkey\Controllers;

use SilexMonkey\Controllers\BaseAPIController;

class TrackingAPIController extends BaseAPIController
{
    protected $anonymousAccess = true;

    public function track()
    {
        $this->accessValidation();

        $trailData = [];
        $actionTrack = [];
        if ( !empty($this->inputData['d']) ) {
            $data = json_decode( base64_decode( $this->inputData['d'] ), true );
            $trailData = $data;

            unset($this->inputData['d']);
        }

        if ( !empty($this->inputData['ex']) ) {
            $trailData['extra'] = $this->inputData['ex'];
            unset($this->inputData['ex']);
        }

        if ( !empty($this->inputData['e']) ) {
            $trailData['eventType'] = $this->inputData['e'];
        }

        if ( !empty($this->inputData['m']) ) {
            $trailData['eventMeta'] = $this->inputData['m'];
        }

        if ( !empty($this->inputData['href']) ) {
            $trailData['clickHref'] = $this->inputData['href'];
        }

        if ( !empty($this->inputData['ww']) ) {
            $trailData['windowWidth'] = (int)$this->inputData['ww'];
            unset($this->inputData['ww']);
        }

        if ( !empty($this->inputData['wh']) ) {
            $trailData['windowHeight'] = (int)$this->inputData['wh'];
            unset($this->inputData['wh']);
        }

        if ( !empty($this->inputData['ph']) ) {
            $trailData['pageHeight'] = (int)$this->inputData['ph'];
            unset($this->inputData['ph']);
        }

        if ( !empty($this->inputData['clk']) ) {
            $temp = explode('-', base64_decode($this->inputData['clk']));
            for($i=0;$i<count($temp);$i++)
            {
                $thisCoordinate = explode(',',$temp[$i]);
                $thisCoordinate[0] = (isset($thisCoordinate[0])?(string)$thisCoordinate[0]:'unknown');
                $thisCoordinate[1] = (isset($thisCoordinate[1])?(int)$thisCoordinate[1]:0);
                $thisCoordinate[2] = (isset($thisCoordinate[2])?(int)$thisCoordinate[2]:0);
                $thisCoordinate[3] = (isset($thisCoordinate[3])?(int)$thisCoordinate[3]:0);
                $thisCoordinate[4] = (isset($thisCoordinate[4])?(int)$thisCoordinate[4]:0);
                $thisCoordinate[5] = (isset($thisCoordinate[5])?(int)$thisCoordinate[5]:0);
                $actionTrack[$i] = $thisCoordinate;
            }
        }

        if (isset($this->inputData['clk'])) {
            unset($this->inputData['clk']);
        }

        if ( !empty($this->inputData['i']) ) {
            $txid = new \MongoId($this->inputData['i']);
            unset($this->inputData['i']);
        }

        $thisTrail = new \SilexMonkey\Models\Trail( $this->app );
        if ( !empty($this->inputData['u']) ) {
            $url = parse_url(urldecode( $this->inputData['u'] ), PHP_URL_PATH);
            $trailData['targetType'] = 'page';
            $trailData['targetId'] = $url;
            unset($this->inputData['u']);
            if ( !empty($this->inputData['t']) ) {
                $trailData['loadTime'] = (float)$this->inputData['t'];
                unset($this->inputData['t']);
            }

            $trailData = array_merge($this->inputData, $trailData);
            $thisTrail->save($trailData, ['_id' => $txid]);
        } else if ( !empty($this->inputData['tp']) ) {
            //To add the timeOnPage to the previously added page data using txid to identify
            $trailData['timeOnPage'] = (int)$this->inputData['tp'];
            unset($this->inputData['tp']);

            $match['_id'] = $txid;

            $trailData = array_merge($this->inputData, $trailData);
            $thisTrail->update($trailData, $match);
            foreach ( $actionTrack as $thisTrack )
            {
                if ( $thisTrack[0] != 'mp' ) {
                    $thisTrail->push(['action'=>$thisTrack], $match);
                }
            }
        } else if ( !empty($this->inputData['e']) ) {
            $match['_id'] = $txid;
            $thisTrail->push(['click'=>$trailData], $match);
        }

        $this->outputData['status'] = 'ok';
        return $this->render();
    }
}
