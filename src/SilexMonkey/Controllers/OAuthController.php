<?php

namespace SilexMonkey\Controllers;

use SilexMonkey\Helpers\OAuthHelper;
use SilexMonkey\Models\Token;

class OAuth extends BaseAPIController
{

    protected function login()
    {
        $this->data = $this->readFormData();

        if ( empty($this->data) ) {
            $this->data['error'] = "Form error detected (" . $this->error . "). E0101";
            return $this->render();
        }

        $thisUser = new \SilexMonkey\Models\User($this->app, $this->data);
        $thisUser->loadSelfFromDB();

        if ( !$thisUser->doesExist() ) {
            $this->data['error'] = "Login not found! (E0201)";
		    return $this->render();
        } else if ( !$thisUser->verifyPassword($this->formData['password']) ) {
            $this->data['error'] = "Password mismatched! (E0202)";
		    return $this->render();
        } else if ( $this->formData['loginId'] != $thisUser->getLoginId() ) {
            $this->data['error'] = "ID mismatched! (E0203)";
		    return $this->render();
        } else if ( 'REGISTERED' === $thisUser->getStatus() ) {
            $this->data['error'] = "Account not verified! (E0204)";
		    return $this->render();
        } else if ( 'VERIFIED' !== $thisUser->getStatus() ) {
            $this->data['error'] = "Account locked out! (E0205)";
		    return $this->render();
        } else {
            $this->data = array();

            $this->data['token'] = OAuthHelper::generateToken();
            $this->data['userId'] = $thisUser->getUserId();

            $thisToken = new \SilexMonkey\Models\Token($app, $this->data);
            $token = $thisToken->create();

            if ( !empty($token) ) {
                return $this->render();
            } else {
                $this->data= array('error' => "Token creation failed! (E0206)");
		        return $this->render();
            }
        }
   
    }

}
