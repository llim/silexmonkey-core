<?php

namespace SilexMonkey\Queue\Job\Image;

use Aws\S3\S3Client;
use SilexMonkey\Helpers\ImageHelper;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class Regenerate extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $success = true;
        $S3Bucket = $this->app['config']['application']['mediaS3Bucket'];

        $client = $this->app['aws']->get('s3');
        $filesystem = new Filesystem(new Adapter($client, $S3Bucket));

        $mediaId = $jobInfo['payload']['mediaId']; 
        $sizes = $jobInfo['payload']['sizes']; 
        $thisUrl = $jobInfo['payload']['sourceUrl'];
        $type = $jobInfo['payload']['type'];

        $thisDate = date('Y-m-d H:i:s');
        echo "\t$thisDate :: Thumbnail Regeneration started for $mediaId\n";
        echo "Processing media of id $mediaId of $type for " . implode(',', $sizes) . " \n";

        $targetFile = "/tmp/" . $mediaId . ".jpg";
        echo "Downloading $thisUrl as $targetFile\n";

        if ( ! file_put_contents($targetFile, fopen($thisUrl, 'r')) ) {
            echo "Error in retrieving $thisUrl! Skip to next media\n";
            $success = false;
        } else {

            if ( !is_file($targetFile) ) {
                $success |= false;
            } else {
                    $imagine = new ImageHelper($targetFile);
                    $imagine->setOutputDirectory( "/tmp/" . $mediaId . "/");
                    foreach ( $sizes as $size ) {
                        $outputFilename = $size . ".jpg";
                        $thisStatus = $imagine->generateThumbnail($size, $outputFilename);
                        if ( !$thisStatus ) {
                            $sucess |= false;
                        } else {
                            $file= "/tmp/" . $mediaId . "/" . $outputFilename ;
                            $leadingBucket = substr($mediaId, -1);
    
                            $stream = fopen($file, 'r+');
                            $targetS3Path = $leadingBucket . "/" . $mediaId . "/" . basename($file);
                            echo "Pushing $file to s3://$S3Bucket/$targetS3Path\n";
    
                            $options = ['s3rr' => true ];

                            $thisSuccess = \SilexMonkey\Helpers\S3::copyToS3($this->app, $file, $targetS3Path, $options);
    
                            if ( $thisSuccess ) {
                                unlink($file);
                            }
    
                            $success |= $thisSuccess;
                        }
                    }
            }
        }

        $thisDate = date('Y-m-d H:i:s');
        echo "\t$thisDate :: Thumbnail Regeneration Completed.\n";
        echo "\t$thisDate :: Memory peak usage: ".number_format(memory_get_peak_usage()/(1024*1024),2)."MB\n";

        if ( $success ) {
            rmdir("/tmp/".$mediaId);
        }
        unlink($targetFile);

        return (!$success) 
                ? array('status' => 'fail', 'message' => "Failed to copy and resize") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'message' => 'Copoy from S3  and resize completed'
                    ) 
                );
    }
}
