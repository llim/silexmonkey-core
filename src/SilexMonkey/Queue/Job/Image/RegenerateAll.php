<?php

namespace SilexMonkey\Queue\Job\Image;

use Aws\S3\S3Client;
use SilexMonkey\Helpers\ImageHelper;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class RegenerateAll extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $success = true;

        $S3Bucket = $this->app['config']['application']['mediaS3Bucket'];
        $sizes = $this->app['config']['application']['thumbnailSizes'];

        $client = $this->app['aws']->get('s3');
        $filesystem = new Filesystem(new Adapter($client, $S3Bucket));
        $media = new \SilexMonkey\Models\MediaModel($this->app);
        $mediaList = $media->retrieve(array('storage'=>'S3'));

        foreach ( $mediaList as $mediaItem ) {
            $mediaId = $mediaItem['_id'];
                 
            $thisMedia = new \SilexMonkey\Models\MediaModel($this->app, array("mediaId" => $mediaId));
            $thisMedia->loadSelfFromDB();
            $thisUrl = $thisMedia->getUrl();

            $targetFile = "/tmp/" . $mediaId . ".jpg";

            echo "Downloading $thisUrl\n";
            if ( ! file_put_contents($targetFile, fopen($thisUrl, 'r')) ) {
                echo "Error in retrieving $thisUrl! Skip to next media\n";
                $success = false;
            } else {

                if ( !is_file($targetFile) ) {
                    $success |= false;
                } else {
                    $imagine = new ImageHelper($targetFile);
                    $imagine->setOutputDirectory( "/tmp/" . $mediaId . "/");
                    foreach ( $sizes as $size ) {
                        $outputFilename = $size . ".jpg";
                        $thisStatus = $imagine->generateThumbnail($size, $outputFilename);
                        if ( !$thisStatus ) {
                            $sucess |= false;
                        } else {
                            $file= "/tmp/" . $mediaId . "/" . $outputFilename ;
                            $leadingBucket = substr($mediaId, -1);
    
                            $stream = fopen($file, 'r+');
                            $targetS3Path = $leadingBucket . "/" . $mediaId . "/" . basename($file);
                            echo "Pushing $file to s3://$S3Bucket/$targetS3Path\n";
    
                            $thisSuccess = \SilexMonkey\Helpers\S3::copyToS3($app, $file, $targetS3Path);
    
                            if ( $thisSuccess ) {
                                unlink($file);
                                unlink("/tmp/".$mediaId);
                            }
    
                            $success |= $thisSuccess;
                        }
                    }
                }
            }
        }

        return (!$success) 
                ? array('status' => 'fail', 'message' => "Failed to copy and resize") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'message' => 'Copoy from S3  and resize completed'
                    ) 
                );
    }
}
