<?php

namespace SilexMonkey\Queue\Job\S3;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class MediaMove extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $client = $this->app['aws']->get('s3');
        $S3Bucket = $this->app['config']['application']['mediaS3Bucket'];
        $filesystem = new Filesystem(new Adapter($client, $S3Bucket));
        $mediaId = $jobInfo['payload']['mediaId'];
        
        $success = true;
        $leadingBucket = substr($mediaId, -1);

        foreach ( $jobInfo['payload']['targetFile'] as $file ) {
            echo date("Y-m-d H:i:s") . "Processing filename: $file \n";
            $stream = fopen($file, 'r+');
                 
            //$baseFilename = basename($file);
            $path = pathinfo($file);
            $baseFilename = $path['basename'];

            $targetS3Path = $leadingBucket . "/" . $mediaId . "/" . $baseFilename;
            if ( preg_match('/^\d+x\d+\.[a-zA-Z0-9]*$/', $baseFilename ) > 0) {
                echo "Pushing $file ($baseFilename) to s3://$S3Bucket/$targetS3Path to S3RR storage\n";
                // if filename match 1024x1224. pattern, then it is a thumbnail, use S3RR instead, cheaper!! 
                $copySuccess = $filesystem->putStream($targetS3Path, $stream, [
                    'visibility' => 'public',
                    'mimetype' => ( strtolower($path['extension']) == "png" ) ? 'image/png' : 'image/jpeg',
                    'StorageClass' => 'REDUCED_REDUNDANCY',
                    'Expires'    => gmdate("D, d M Y H:i:s T", strtotime("+3 years")),
                    'Cache-Control' => 'public,max-age=93312000',
                    'Metadata' => ['Cache-Control' => 'public,max-age=93312000']
                    ]);
            } else {
                echo "Pushing $file ($baseFilename) to s3://$S3Bucket/$targetS3Path to standard S3 storage\n";
                $copySuccess = $filesystem->putStream($targetS3Path, $stream, [
                    'visibility' => 'public',
                    'mimetype' => ( strtolower($path['extension']) == "png" ) ? 'image/png' : 'image/jpeg',
                    'Expires'    => gmdate("D, d M Y H:i:s T", strtotime("+3 years")),
                    'Cache-Control' => 'public,max-age=93312000',
                    'Metadata' => ['Cache-Control' => 'public,max-age=93312000']
                    ]);
            }

            $thisSuccess = $filesystem->has($targetS3Path);

            if ( $thisSuccess ) {
                unlink($file);
            }
            $success |= $thisSuccess;
        }

        if ( $success ) {
            $dir = dirname($file);
            if ( count(scandir($dir)) <= 2 ) { 
                rmdir($dir);
            } else {
                echo "$dir is not empty, not removing...\n";
            }

            echo date("Y-m-d H:i:s") . "Updating Media information in DB\n";
            $thisMedia = new \SilexMonkey\Models\MediaModel($this->app, array("mediaId" => $mediaId));
            $thisMedia->update(array("storage"=>"S3", "S3Bucket" => $S3Bucket, "s3Ready" => true));
        }
        
        return (!$success) 
                ? array('status' => 'fail', 'message' => "Move to S3 failed") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'message' => 'Move to S3 completed'
                    ) 
                );
    }
}
