<?php

namespace SilexMonkey\Queue\Job\S3;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class CopyFrom extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $success = true;
        $leadingBucket = substr($mediaId, -1);
        $targetDir = $jobInfo['payload']['targetDir'] ;
        if ( is_dir($targetDir) ) {
            mkdir($targetDir, true);
        }

        foreach ( $jobInfo['payload']['MediaIds'] as $mediaId ) {
                 
            $thisMedia = new \SilexMonkey\Models\MediaModel($this->app, array("mediaId" => $mediaId));
            $thisMedia->loadSelfFromDB();
            $thisUrl = $thisMedia->getUrl();

            $targetFile = $targetDir . "/" . $mediaId . ".jpg";

            echo "Downloading $thisUrl\n";
            file_put_contents($targetFile, fopen($thisUrl, 'r'));

            if ( !is_file($targetFile) ) {
                $success |= false;
            }
        }

        return (!$success) 
                ? array('status' => 'fail', 'message' => "Failed to copy some file from S3") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'message' => 'Copt from S3 completed'
                    ) 
                );
    }
}
