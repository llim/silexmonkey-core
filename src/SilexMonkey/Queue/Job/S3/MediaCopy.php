<?php

namespace SilexMonkey\Queue\Job\S3;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v2\AwsS3Adapter as Adapter;

class MediaCopy extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $client = $this->app['aws']->get('s3');

        $S3Bucket = $this->app['config']['application']['mediaS3Bucket'];

        $filesystem = new Filesystem(new Adapter($client, $S3Bucket));
        $mediaId = $jobInfo['payload']['mediaId'];
        
        $success = true;
        $leadingBucket = substr($mediaId, -1);

        foreach ( $jobInfo['payload']['targetFile'] as $file ) {
        }
        
        return (!$success) 
                ? array('status' => 'fail', 'message' => "Move to S3 failed") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'message' => 'Move to S3 completed'
                    ) 
                );
    }
}
