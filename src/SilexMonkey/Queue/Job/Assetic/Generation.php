<?php

namespace SilexMonkey\Queue\Job\Assetic;

class Generation extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $asset = $jobInfo['payload']['asset'];

        echo date("Y-m-d H:i:s") . ": starting assetic generation for $asset\n";  

        $success = \SilexMonkey\Helpers\Assetic::CompileCollection($this->app, $asset);

        if ( $success ) {
            echo date("Y-m-d H:i:s") . ": removing $asset from generation\n";  
            $this->app['redis']->sRemove('assetic|generation', $asset);
        }
        
        return (!$success) 
                ? array('status' => 'fail', 'message' => "Asset generation for $asset failed") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'message' => 'Asset generation for '. $asset . ' completed'
                    ) 
                );
    }
}
