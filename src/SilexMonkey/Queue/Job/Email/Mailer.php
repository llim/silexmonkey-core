<?php

namespace SilexMonkey\Queue\Job\Email;

class Mailer extends \SilexMonkey\Queue\Job\BaseJob 
{
    public function realPerform( $jobInfo )
    {
        $id = $this->app['config']['sendgrid']['id'];
        $password = $this->app['config']['sendgrid']['password'];
        $sendgrid = new \SendGrid($id, $password);

        if ( empty($jobInfo['payload']['to']) ) {
            return array('status' => 'fail', 'message' => "Empty receipient") ;
        }

        $to = $jobInfo['payload']['to'];
        $subject = $jobInfo['payload']['subject'];
        $body = $jobInfo['payload']['body'];
        $from = !empty($jobInfo['payload']['from']) ? $jobInfo['payload']['from'] : $this->app['config']['sendgrid']['from'];
        $replyto = !empty($jobInfo['payload']['replyto']) ? $jobInfo['payload']['replyto'] : null;

        $sendStatus = \SilexMonkey\Helpers\SendGrid\Mailer::sendMail($sendgrid, $from, $to, $subject, $body, $replyto);

        return $sendStatus
                ? array(
                    'status' => 'ok', 
                    'output' => array( 'message' => "email to $to sent") 
                )
                : array(
                    'status' => 'fail',
                    'message' => "Failed to send out email to $to : " . \SilexMonkey\Helpers\SendGrid\Mailer::getLastError()
                ) ;
    }
}
