<?php

namespace SilexMonkey\Queue\Job;

class JobFactory {
    public static function get(\SilexMonkey\Queue\QueueMaster $queueMaster, $jobClass, \Silex\Application &$app, $namespace = '\\SilexMonkey\\Queue\\Job')
    {
        if ( class_exists($jobClass) ) {
            return new $jobClass($queueMaster, $app);
        } else {
            $class = empty($namespace) ? $jobClass : $namespace . '\\' . $jobClass;
            if ( class_exists( $class ) ) {
                return new $class($queueMaster, $app);
            } else {
                return null;
            }
        }
    }
}
