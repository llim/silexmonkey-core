<?php

namespace SilexMonkey\Queue\Job;

class DummyJob extends BaseJob
{
    public function realPerform( $jobInfo )
    {
        $mySleepTime = rand(1,10);
        echo "My Sleep time = $mySleepTime \n";
        sleep($mySleepTime);
        $fail = rand(1,100) <= 5;
        
        return $fail 
                ? array('status' => 'fail', 'message' => 'I do not know why I failed') 
                : array('status' => 'ok', 'output' => array('time' => time()), 'message' => 'job done!');
    }
}
