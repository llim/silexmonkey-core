<?php

namespace SilexMonkey\Queue\Job;

use SilexMonkey\Helpers\ImageHelper;

class ThumbnailGeneration extends BaseJob 
{
    public function realPerform( $jobInfo )
    {
        echo date("Y-m-d H:i:s") . " Started thumbnail generation";       
        $basePath = $this->app['config']['application']['mediaLocalPath'] . "/" . $jobInfo['payload']['mediaId'] . "/";
        if ( !file_exists($basePath) ) {
            echo "creating $basePath";
            mkdir( $basePath );
        }

        if ( empty( $jobInfo['payload']['sourceStorage'] ) || (!empty($jobInfo['payload']['sourceStorage']) && ( $jobInfo['payload']['sourceStorage'] == 'localdisk' )) ) { 
            echo "using file in local disk";
            $imagine = new ImageHelper($jobInfo['payload']['source']);
        } else if ( !empty($jobInfo['payload']['sourceStorage']) && ( $jobInfo['payload']['sourceStorage'] == 's3' ) ) {
            echo "Downloading file from s3";
            $client = $this->app['aws']->get('s3');
            $S3Bucket = $this->app['config']['application']['mediaS3Bucket'];
            $mediaLocalPath = $this->app['config']['application']['mediaLocalPath'];

            $mediaId = $jobInfo['payload']['mediaId'];
            $localPath =  $basePath . basename( $jobInfo['payload']['source'] );

            $result = $client->getObject(array(
                'Bucket' => $S3Bucket,
                'Key'    => $jobInfo['payload']['source'],
                'SaveAs' => $localPath
            ));
            
            $imagine = new ImageHelper($localPath);
        }

        $imagine->setOutputDirectory( $basePath );
        $fail = false;
        $failedSizes = array();
        $generated = array();
        foreach ( $jobInfo['payload']['sizes'] as $size ) {
            $outputFilename = $size . ".jpg";
            $method = ( !empty($jobInfo['payload']['method']) ) ? $jobInfo['payload']['method'] : 'crop';
            echo "Generating thumbnail using $method\n";
            $thisStatus = $imagine->generateThumbnail($size, $outputFilename, $method);
            if ( !$thisStatus ) {
                $failedSizes[] = $size;
                $fail |= true;
            } else {
                $generated[] = $basePath . $outputFilename ;
            }
        }
        
        $failedGeneration = implode(",", $failedSizes);

        if ( !$fail ) {
            //move original to this location
            $thisPath = pathinfo( $localPath ) ;
            $newOriginalLocation = $basePath . $jobInfo['payload']['mediaId'] . "." . $thisPath['extension'];
            echo "All thumbnail generated, so moving original from " . $jobInfo['payload']['source'] . " to " . $newOriginalLocation . "\n";
            rename( $localPath, $newOriginalLocation );
            $generated[] = $newOriginalLocation;
        } 

        echo date("Y-m-d H:i:s") . " Thumbnail Generation done";
        return $fail 
                ? array('status' => 'fail', 'message' => "Thumbnail ($failedGeneration) generation failed") 
                : array(
                    'status' => 'ok', 
                    'output' => array(
                        'mediaId' => $jobInfo['payload']['mediaId'],
                        'targetFile' => $generated,
                        'message' => 'Thumbnail generation completed'
                    ) 
                );
    }
}
