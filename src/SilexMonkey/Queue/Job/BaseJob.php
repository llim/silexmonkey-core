<?php

namespace SilexMonkey\Queue\Job;

abstract class BaseJob {
    protected $id = null;
    protected $queueMaster = null;
    protected $app = null;
    public $fatalOnError = false;

    public function __construct(\SilexMonkey\Queue\QueueMaster $qMaster, \Silex\Application &$app)
    {
        $this->queueMaster = $qMaster;
        $this->app = $app;
    }

    abstract public function realPerform( $jobPayload );

    public function setUp()
    {
        return true;
    }

    public function tearDown()
    {
        return true;
    }

    public function perform($jobInfo)
    {
        if ( !$this->setUp() ) {
            $this->queueMaster->updateJobStatus($jobInfo['_id'], 'FAILED', 'Job Setup failed');
        }

        $workResult = $this->realPerform($jobInfo);

        if ( $workResult['status'] == 'ok' ) {
            $message = (!empty($workResult['message'])) ? $workResult['message'] :'';
            unset( $workResult['message'] );
            if ( !empty($jobInfo['following']) ) { 
                $following = $jobInfo['following'];
                $nextJob = array_shift( $following );
                if ( !empty($workResult['output'] ) ) {
                    $nextJob['payload'] = array_merge($nextJob['payload'], $workResult['output']);
                }
                $nextJob['_id'] = $this->queueMaster->addJob($nextJob['class'], $nextJob['payload'], $following);
                $message .= ", queued next job (" . $nextJob['_id'] . ")";
            }
            $this->queueMaster->updateJobStatus($jobInfo['_id'], 'DONE', $message);
        } else {
            $message = $workResult['message'] ? :'';
            $this->queueMaster->updateJobStatus($jobInfo['_id'], 'FAILED', $message);
        }

        if ( !$this->tearDown() ) {
            $this->queueMaster->updateJobStatus($jobInfo['_id'], 'DONE', 'Job teardown failed');
        }

        return $workResult;
    }
}
