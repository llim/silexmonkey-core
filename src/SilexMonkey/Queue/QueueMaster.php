<?php

namespace SilexMonkey\Queue;

class QueueMaster {
    private $mongoServer = 'localhost';
    private $mongoPort = 27017;
    private $mongoDb = 'sample';
    private $redisServer = 'localhost';
    private $redisPort = 6379;
    private $queue = 'default';

    private $mongo = null;
    private $mongoCollection = null;
    private $redis = null;

    public function __construct($mongoServer, $mongoPort, $mongoDb, $redisServer, $redisPort, $queue=null)
    {
        $this->mongoServer = $mongoServer;
        $this->mongoPort = $mongoPort;       
        $this->mongoDb = $mongoDb;       
        $this->redisServer = $redisServer;
        $this->redisPort = $redisPort;       
        $this->queue = $queue?:'default';

        $this->mongo = new \MongoClient("mongodb://".$this->mongoServer.":".$this->mongoPort);
        $this->mongoCollection = $this->mongo->selectDB($this->mongoDb)->selectCollection("bgjobs");

        $this->redis = new \Redis();
        $this->redis->connect($this->redisServer, $this->redisPort);
    }

    public function setQueue($queue)
    {
        $this->queue = $queue;
    }

    public function getQueue()
    {
        return $this->queue ;
    }

    public function addJob($jobClass, $jobPayload, $following = array())
    {
        $jobId = new \MongoId();
        $jobCreatedTS = time();
        $jobIdString = $jobId . '';
        $thisJob = array(
            '_id' => $jobIdString,
            'queue' => $this->queue,
            'status' => 'NEW',
            'class' => $jobClass,
            'payload' => $jobPayload,
            'following' => $following,
            'createdTS' => $jobCreatedTS
        );
        
        $queueStatus = $this->addJobToQueue( json_encode($thisJob) );

        if ( $queueStatus > 0 ) {
            $thisJob['status'] = 'QUEUED';
        }

        // use real mongoId object now
        $thisJob['_id'] = $jobId;
        $thisJob['createdTS'] = new \MongoDate($jobCreatedTS);
        $this->mongoCollection->insert( $thisJob );

        return $thisJob['_id'] . '';
    }

    private function addJobToQueue($job)
    {
        return $this->redis->rPush($this->queue, $job);
    }

    public function getJob()
    {
        $result = $this->redis->blPop($this->queue, 30);
        if ( is_array($result) && !empty($result[1]) ) {
            return json_decode($result[1], true );
        } else {
            return null;
        }
    }

    public function updateJobStatus($jobId, $jobStatus, $logMessage = null)
    {
        return 
            (!empty($logMessage)) 
            ?  $this->mongoCollection->update(
                    array('_id' => new \MongoId($jobId)),
                    array(
                        '$set' => array('status' => $jobStatus),
                        '$push' => array('log' => $logMessage)
                    )
                ) 
            : $this->mongoCollection->update(
                    array('_id' => new \MongoId($jobId)),
                    array('$set' => array('status' => $jobStatus))
                ) 
        ;
    }

    public function requeueJob($jobId)
    {
        $jobInfo = $this->mongoCollection->findOne(array('_id' => new \MongoId($jobId)));   
        $jobInfo['_id'] = $jobInfo['_id'] . '';
        $jobInfo['createdTS'] = $jobInfo['createdTS']->sec;
        $queueStatus = $this->addJobToQueue( json_encode($jobInfo) );

        if ( $queueStatus > 0 ) {
            return $this->updateJobStatus($jobInfo['_id'], 'REQUEUED', 'Requeued success');
        } else {
            return $this->updateJobStatus($jobInfo['_id'], 'FAILEd', 'Requeued failed');
        }
    }

    public function getQueueSize($queue)
    {
        return $this->redis->lLen($queue);
    }

    public function getOldestJobAge($queue)
    {
        $now = time();
        return $now - (json_decode($this->redis->lGet($queue, 0), true)['createdTS'] ? : $now );
    }
}

