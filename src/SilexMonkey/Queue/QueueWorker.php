<?php

namespace SilexMonkey\Queue;

class QueueWorker {
    private $queueMaster = null;
    private $app = null;
    private $baseSleepTime = 5;
    private $maxSleepTime = 160;
    private $debug = false;

    public function __construct(\Silex\Application &$app)
    {
        $this->queueMaster = $app['queue'];       
        $this->app = $app;       
        if ( !empty( $app['config']['queue']['baseSleepTime'] ) ) {
            $this->baseSleepTime = $app['config']['queue']['baseSleepTime'];
        }
        if ( !empty( $app['config']['queue']['maxSleepTime'] ) ) {
            $this->maxSleepTime = $app['config']['queue']['maxSleepTime'];
        }
        if ( !empty( $app['config']['queue']['debug'] ) ) {
            $this->debug = $app['config']['queue']['debug'];
        }
    }

    public function getJob()
    {
        return $this->queueMaster->getJob();
    }

    public function working()
    {
        $sleepCounter = 0;

        while(true) {
            $thisJobInfo = $this->getJob();
            if ( !empty($thisJobInfo) ) {
                $t1 = microtime(true);
                $sleepCounter = 0;
                $thisJob = \SilexMonkey\Queue\Job\JobFactory::get($this->queueMaster, $thisJobInfo['class'], $this->app);
                if ( !empty($thisJob) ) {
                    $result = $thisJob->perform($thisJobInfo);
                    if ( $result['status'] != 'ok' && $thisJob->fatalOnError ) {
                        die($thisJobInfo['_id'] . ' of ' . $thisJobInfo['class'] . ' encountered error and committed suicide!');
                    }
                } else {
                    $this->queueMaster->updateJobStatus($thisJobInfo['_id'], 'UNKNOWN', 'Unknown class');
                }
                $jobDuration = microtime(true) - $t1;
                echo date("Y-m-d H:i:s") . ": " . $thisJobInfo['_id'] . ' of ' . $thisJobInfo['class'] . ' completed in ' . sprintf("%.4fsecs", $jobDuration) . "\n"; 
            } else {
               /* $sleepTime = $this->baseSleepTime * pow(2,$sleepCounter);
                $sleepTime = ( $sleepTime >= $this->maxSleepTime ) ? $this->maxSleepTime : $sleepTime; 
                if ( $this->debug ) {
                    echo "sleep for $sleepTime \n";
                }
                sleep($sleepTime);
                $sleepCounter++; */
            }
        }
    }
}
