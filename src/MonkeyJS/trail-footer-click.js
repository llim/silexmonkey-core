function sender(b, c) {
    var a = new XMLHttpRequest();
    a.onreadystatechange = function() {
        if (a.readyState == 2) {
            a.abort()
        }
    };
    a.open("GET", b, true);
    a.timeout = c || 50;
    a.send()
}

function sendNow() {
    sender("/v0/track?tp=" + Math.round((performance.now() - window.olt) / 1000) + (txid ? ("&i=" + txid) : "") + "&clk=" + window.btoa(window.c.join("-")), 80);
    window.c = []
}

function mark(c, b, a) {
    var e = 0,
        d = 0;
    if (typeof(window.pageYOffset) == "number") {
        d = window.pageYOffset;
        e = window.pageXOffset
    } else {
        if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
            d = document.body.scrollTop;
            e = document.body.scrollLeft
        }
    }
    if (e != window.lastC[0] || d != window.lastC[1] || b != window.lastC[2] || a != window.lastC[3] || c == "mp") {
        window.c.push([c, Math.round((performance.now() - window.olt) / 1000), e, d, b, a].join());
        window.lastC = [e, d, b, a]
    }
}

function mouseMove(f) {
    var g, d, a, c = f.pageX,
        b = f.pageY;
    f = f || window.event;
    if (f.pageX == null && f.clientX != null) {
        g = (f.target && f.target.ownerDocument) || document;
        d = g.documentElement;
        a = g.body;
        c = f.clientX + (d && d.scrollLeft || a && a.scrollLeft || 0) - (d && d.clientLeft || a && a.clientLeft || 0);
        b = f.clientY + (d && d.scrollTop || a && a.scrollTop || 0) - (d && d.clientTop || a && a.clientTop || 0)
    }
    mark("c", c, b)
}
if ("onpagehide" in window) {
    window.addEventListener("pagehide", function(a) {
        if (a.persisted) {
            return
        }
        sendNow()
    }, false)
} else {
    window.onbeforeunload = function(a) {
        senderNow()
    }
}
$(function() {
    window.olt = performance.now() || 0;
    window.c = [];
    window.lastC = [];
    document.addEventListener("click", mouseMove, false);
    if (performance.now()) {
        var d = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
        var c = encodeURIComponent(document.referrer);
        sender("/v0/track?u=" + encodeURIComponent(document.URL) + "&t=" + (Math.round((window.olt - t1) * 1000) / 1000) + "&ph=" + d + "&ww=" + window.innerWidth + "&wh=" + window.innerHeight + "&ref=" + c + (txid ? ("&i=" + txid) : ""))
    }
    var b = setInterval(function() {
        mark("t", 0, 0)
    }, 500);
    var a = setInterval(function() {
        mark("mp", 0, 0);
        sendNow()
    }, 15100);
    setTimeout(function() {
        clearInterval(b);
        clearInterval(a)
    }, 3600000);
    $("body").on("click contextmenu touchstart", "a,button", function(k) {
        var g = $(this).data("track");
        var i = k.type;
        var h = encodeURIComponent($(this).attr("href"));
        var f = $(this).data("extra");
        var j = k || window.event;
        if (h || g || f) {
            sender("/v0/track?" + (g ? ("d=" + g + "&") : "") + "href=" + h + "&e=" + i + ((j.ctrlKey || j.metaKey) ? "&m=c" : "") + (f ? "&ex=" + f : "") + (txid ? ("&i=" + txid) : ""))
        }
    })
});
