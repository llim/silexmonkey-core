# SilexMonkey Core

SilexMonkey is a system framework that based on Silex Microframework, designed to utilize various popular and proven database technology and cloud services, to enable rapid system building and allow developer to focus on building application first, and yet allow for vertical and horizontal scaling throughout application plus data storage layer.  The framework is designed to use these technology and cloud services:

  - [AWS]: S3, CloudFront
  - [MongoDB]
  - [Redis]
  - [Elastic Search]
  - [Composer]
  
Also included adaptor for these cloud services:

  - [SendGrid] (Transactional Email Service)
  - Twilio (SMS)
  - Paypal (Payment Gateway)
  - NewRelic (Cloud based monitoring)

This is a work in progress, with many aspects to be fine tuned for better testability.  Furthermore, documentation effort just started.

### Version
1.0.0

### Installation

To setup your application:

First, install composer on your machine and setup your repository for your application. Then copy the composer.json.sample as composer.json.

```sh
$ composer install
```

Then core libraries needed will be pulled into and several directory will be setup by the installation script.

License
----

MIT

[Silex]:http://silex.sensiolabs.org/
[AWS]:http://aws.amazon.com/
[MongoDB]:http://www.mongodb.com/
[Elastic Search]:http://www.elasticsearch.org/guide/
[Redis]:http://redis.io/
[SendGrid]:https://sendgrid.com/
[Composer]:https://getcomposer.org/
