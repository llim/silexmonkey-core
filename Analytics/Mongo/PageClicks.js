/* generic clicks per page aggregation */
db.trails.aggregate(
    {
        '$match':{
            'click':{'$exists':1}
        }
    },
    {
        '$project':{
            'page': '$targetId',
            'clicks': {'$size':'$click'}
        }
    },
    {
        '$group': {
            '_id': '$page',
            'clicks': {'$sum': '$clicks'}
        }
    },
    {
        '$sort': {
            'clicks': -1
        }
    }
);
