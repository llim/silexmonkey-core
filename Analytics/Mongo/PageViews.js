/* generic clicks per page aggregation */
db.trails.aggregate(
    {
        '$match':{
            'targetType': 'page'
        }
    },
    {
        '$project':{
            'page': '$targetId',
            'clicks': {'$ifNull':['$click',[]]}
        }
    },
    {
        '$project':{
            'page': '$page',
            'clicks': {'$size':'$clicks'}
        }
    },
    {
        '$group': {
            '_id': '$page',
            'pageviews': {'$sum':1},
            'clicks': {'$sum': '$clicks'}
        }
    },
    {
        '$project':{
            'page': '$targetId',
            'pageviews': '$pageviews',
            'clicks': '$clicks',
            'ctr': {'$divide': ['$clicks', '$pageviews' ] }
        }
    },
    {
        '$sort': {
            'ctr': -1
        }
    }
);
