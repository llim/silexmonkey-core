m = function() {

    var iphone = /iphone/i;
    var bb = /BB/;
    var bb2 = /BlackBerry/;
    var ipad = /ipad/i;
    var android = /android/i;
    var gbot = /googlebot/i;
    var opera = /opera/i;
    var symbian = /symbian/i;

    if (this.context
        && this.context.userAgent
    ) {
        ua = iphone.test(this.context.userAgent) ? 'iphone'
            : ipad.test(this.context.userAgent) ? 'ipad'
            : bb.test(this.context.userAgent) ? 'bb'
            : bb2.test(this.context.userAgent) ? 'bb'
            : gbot.test(this.context.userAgent) ? 'googlebot'
            : android.test(this.context.userAgent) ? 'android'
            : opera.test(this.context.userAgent) ? 'opera'
            : symbian.test(this.context.userAgent) ? 'symbian'
            : 'others';

        var screenSize = (this.windowWidth ? this.windowWidth : 0)
                        + 'x'
                        + (this.windowHeight ? this.windowHeight : 0);

        emit({
                ua: ua,
                screenSize: screenSize,
            }, { count: 1 }
        );
    }
}

r = function(keys, values) {
    var count = 0;
    values.forEach(function(v) {
        count += v['count'];
    });

    return {
        count: count
    };
}

db.trails.mapReduce(m, r, {out: "DeviceScreen"});
db.DeviceScreen.find().sort({'value.count':-1});
