/* aggregate by bcookie and session id */

db.trails.aggregate({
    '$group': {
        '_id': '$context.b',
        'session': {
            '$addToSet': '$context.session.id'
        }
    }
}, {
    '$project': {
        'sessionCount': {
            '$size': '$session'
        }
    }
}, {
    '$sort': {
        'sessionCount': -1
    }
});

/* aggregate by bcookie and ip */

db.trails.aggregate({
    '$group': {
        '_id': '$context.b',
        'ip': {
            '$addToSet': '$context.clientIP'
        }
    }
}, {
    '$project': {
        'ipCount': {
            '$size': '$ip'
        }
    }
}, {
    '$sort': {
        'ipCount': -1
    }
});
