m = function() {

    var iphone = /iphone/i;
    var bb = /BB/;
    var bb2 = /BlackBerry/;
    var ipad = /ipad/i;
    var android = /android/i;
    var gbot = /googlebot/i;
    var opera = /opera/i;

    var symbian = /symbian/i;
    var google = /google/i;
    var nth = /nthhotels.com/i;

    if (this.context
        && this.context.userAgent
    ) {

        ua = iphone.test(this.context.userAgent) ? 'iphone'
            : ipad.test(this.context.userAgent) ? 'ipad'
            : bb.test(this.context.userAgent) ? 'bb'
            : bb2.test(this.context.userAgent) ? 'bb'
            : gbot.test(this.context.userAgent) ? 'googlebot'
            : android.test(this.context.userAgent) ? 'android'
            : opera.test(this.context.userAgent) ? 'opera'
            : symbian.test(this.context.userAgent) ? 'symbian'
            : 'others';

        referer = /^$/.test(this.context.referer) ? "none"
            : google.test(this.context.referer) ? "google"
            : nth.test(this.context.referer) ? "nth"
            : "others";

        emit({
                month: this.createdTS.getMonth() + 1,
                day: this.createdTS.getDate(),
                ua: ua,
                r: referer,
            }, { count: 1 }
        );
    }
}

r = function(keys, values) {
    var count = 0;
    values.forEach(function(v) {
        count += v['count'];
    });

    return {
        count: count
    };
}

db.trails.mapReduce(m, r, {out: "UserAgentByDay"});
