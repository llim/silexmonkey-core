m = function() {
    var iphone = /iphone/i;
    var bb = /BB/;
    var bb2 = /BlackBerry/;
    var ipad = /ipad/i;
    var android = /android/i;
    var gbot = /googlebot/i;
    var opera = /opera/i;
    var symbian = /symbian/i;

    if ( (this.createdTS > ISODate("2015-08-06T16:00:00Z")) && this.action && this.action.length > 0 ) {
        var ua = 'others';

        if ( this.context && this.context.userAgent ) {
            ua = iphone.test(this.context.userAgent) ? 'iphone'
                : ipad.test(this.context.userAgent) ? 'ipad'
                : bb.test(this.context.userAgent) ? 'bb'
                : bb2.test(this.context.userAgent) ? 'bb'
                : gbot.test(this.context.userAgent) ? 'googlebot'
                : android.test(this.context.userAgent) ? 'android'
                : opera.test(this.context.userAgent) ? 'opera'
                : symbian.test(this.context.userAgent) ? 'symbian'
                : 'others';
        }

        var actionCount = this.action.length;
        var maxVscroll = 0;
        for(var i=0;i<actionCount;i++)
        {
            var thisAction = this.action[i];
            if ( thisAction.length > 3 ) {
                maxVscroll = ( (maxVscroll < thisAction[3] ) ? thisAction[3] : maxVscroll );
            }
        }

        emit({
                page: this.targetId,
                ua: ua,
                windowWidth: this.windowWidth,
                windowsHeight: this.windowHeight
            }, {
                scroll: maxVscroll,
                timeOnPage: this.timeOnPage
            }
        );
    }
}

r = function(keys, values) {
    var count = 0;
    var totalVerticalScroll = 0;
    var totalTimeOnPage = 0;
    values.forEach(function(v) {
        count++;
        totalVerticalScroll += v['scroll'];
        totalTimeOnPage += v['timeOnPage'];
    });

    var avgVerticalScroll = ( (count > 0 && totalVerticalScroll > 0) ? (totalVerticalScroll/count) : 0 ) ;
    var avgTimeOnPage = ( (count > 0 && totalTimeOnPage > 0) ? (totalTimeOnPage/count) : 0 ) ;

    return {
        scroll: avgVerticalScroll,
        timeOnPage: avgTimeOnPage,
        count: count
    };
}

f = function (key, reducedVal) {
    if ( !reducedVal.count ) {
        reducedVal.count = 1;
    }
    return reducedVal;
}

db.trails.mapReduce(m, r, {finalize: f, out: "PageScrollAnalysis"});

/* find page that user scrolled */
db.PageScrollAnalysis.find({'value.scroll':{'$gt':0}});

/* per page time on page stats */
db.PageScrollAnalysis.aggregate({'$group':{'_id':'$_id.page', 'maxTimeOnPage':{'$max':'$value.timeOnPage'},'avgTimeOnPage':{'$avg':'$value.timeOnPage'}, 'pageView':{'$sum':'$value.count'}}});

db.PageScrollAnalysis.aggregate({
    '$group':{
        '_id':'$_id.page',
        'maxTimeOnPage':{'$max':'$value.timeOnPage'},
        'avgTimeOnPage':{'$avg':'$value.timeOnPage'},
        'maxScroll':{'$max':'$value.scroll'},
        'avgScroll':{'$avg':'$value.scroll'},
        'pageView':{'$sum':'$value.count'}
        }
    }
);
