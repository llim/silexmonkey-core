# features/home.feature
Feature: homepage
  In order to see homepage
  As a website user
  I need to be able to load the homepage

Scenario: homepage
  Given I am on the homepage
  Then the response status code should be 200
  And I should see "The Most Awesomest website ever!?!"
