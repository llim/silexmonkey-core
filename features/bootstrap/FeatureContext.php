<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext,
    Behat\MinkExtension\Context\MinkDictionary;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class FeatureContext extends BehatContext
{
    use MinkDictionary;

    protected $driver;
    protected $sessionId;
    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
        $this->useContext('api',
                new \Behat\CommonContexts\WebApiContext($parameters['base_url'])
            );
        $this->sessionId = $parameters['sessionId'];
    }

    /** @Given /^I am login as admin$/ */
    public function iAmLoginAsAdmin()
    {
        if ( !$this->getSession()->isStarted() ) {
            // start session:
            $this->getSession()->start();
        }

        $this->getSession()->setCookie('PHPSESSID',$this->sessionId);
    }

    /** @Then /^I see this page$/ */
    public function iSeeThisPage()
    {
        echo $this->getSession()->getPage()->find('css','body')->getText();
    }
}
