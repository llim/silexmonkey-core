<?php

use SilexMonkey\Models\BaseModel as BaseModel;

class BaseModelTest extends PHPUnit_Framework_TestCase
{
	protected $baseModel;
	protected $testData;

	public function setup()
	{
		$this->baseModel = $this->getMockForAbstractClass('\SilexMonkey\Models\BaseModel');
		$this->testData = array(
			"integerField" => 1,
			"decimalField" => 1.2,
			"stringField" => "string",
			"arrayField" => array(1,2,3)
			);

        $this->baseModel->expects($this->any())
             ->method('doesExist')
             ->will($this->returnValue(TRUE));
	}

    public function testDoesExist()
    {
        $this->assertTrue($this->baseModel->doesExist());
    }

    public function testInitModel()
    {
    	$this->baseModel->initModel($this->testData);
        $this->assertTrue(is_int($this->baseModel->getIntegerField()));
        $this->assertTrue(is_numeric($this->baseModel->getDecimalField()));
        $this->assertEquals(1,$this->baseModel->getIntegerField());
        $this->assertEquals(1.2,$this->baseModel->getDecimalField());
        $this->assertEquals("string",$this->baseModel->getStringField());
        $this->assertTrue(is_array($this->baseModel->getArrayField()));
    }

    public function testAttachApp()
    {
    	$app = new \Silex\Application();
    	$this->baseModel->attachApp($app);
    	$this->assertEquals("Silex\Application", get_class($this->baseModel->getAttribute('app')));
    }

    public function testDetachApp()
    {
    	$app = new \Silex\Application();
    	$this->baseModel->attachApp($app);
    	$this->assertEquals("Silex\Application", get_class($this->baseModel->getAttribute('app')));
    	$this->baseModel->detachApp();
    	$this->assertNull($this->baseModel->getAttribute('app'));
    }
}