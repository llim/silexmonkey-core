<?php

$autoloader = require(dirname(__DIR__) . '/app/bootstrap.php');
$console = \SilexMonkey\Console::boot(dirname(__DIR__));

$console->run();
